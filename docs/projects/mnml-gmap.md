---
title: "Minimal Google Map"
releaseDate: 2021-05-15

prev:
  text: "Fibonacci Card 🃏"
  link: "/projects/fibonacci-card"
next:
  text: "[DIY] State Management - Svelte 🌀"
  link: "/projects/diy-stores"
---

# {{ $frontmatter.title }} <Badge type="tip">react</Badge><Badge type="tip">vue</Badge><Badge type="tip">typescript</Badge><Badge type="tip">googlemaps</Badge>

**[@dmnchzl/mnml-gmap-react](https://www.npmjs.com/package/@dmnchzl/mnml-gmap-react/)** and **[@dmnchzl/mnml-gmap-vue](https://www.npmjs.com/package/@dmnchzl/mnml-gmap-vue/)** are components to simply inject [Google Maps](https://maps.google.com) 🌍 inside your projet.

These libraries are published under the Beerware license, which means you can do whatever you want with the code.

These projects are powered by [ViteJS](https://vitejs.dev).

## How To Use

First of all, you need to install the package:

With **React**:

```
  npm install @dmnchzl/mnml-gmap-react
```

With **Vue**:

```
  npm install @dmnchzl/mnml-gmap-vue
```

Then, use the component as these samples below:

The **React** way:

```js
import React from "react";
import { GMap } from "@dmnchzl/mnml-gmap-react";
import styleArray from "./styleArray";

/**
 * The 'keyke' (Key + Fake = Cake) is a lie
 * NB: Worst pun ever; i know that...
 */
const API_KEY = "01wNJmwnZEabSeRBV4vQRV1JMUx1MvyMyA6edTN49PF/yf0aUjY2F4NZfIOq/Yh7x0d38+21sEP6Tck/jEmXdw==";
const EIFFEL_TOWER = { lat: 48.8619, lng: 2.2945 };
const BIG_BEN = { lat: 51.5021, lng: -0.1242 };
const COLISEUM = { lat: 41.8961, lng: 12.4879 };

function App() {
  return (
    <div className="container">
      <GMap
        apiKey={API_KEY}
        defaultCenter={EIFFEL_TOWER}
        defaultZoom={5}
        markers={[
          { ...EIFFEL_TOWER, onClick: () => console.log("Eiffel Tower") },
          { ...BIG_BEN, onClick: () => console.log("Big Ben") },
          { ...COLISEUM, onClick: () => console.log("Coliseum") }
        ]}
        styledMap={styleArray}
        enableUI
      />
    </div>
  );
}
```

The **Vue** way:

```html
<template>
  <div class="container">
    <GMap
      :apiKey="apiKey"
      :defaultCenter="location"
      :defaultZoom="5"
      :markers="markers"
      :styledMap="styleArray"
      :enableUI="true" />
  </div>
</template>

<script>
  import { GMap } from "@dmnchzl/mnml-gmap-vue";
  import styleArray from "./styleArray";

  /**
   * The 'keyke' (Key + Fake = Cake) is a lie
   * NB: Worst pun ever; i know that...
   */
  const API_KEY = "YLzo6G7hJxAg55x/EvkEaPTvoO/PhQ9ASxzZ5A6OsNJLE4dxq+zK2lJ5Ta7/z5bbtQ9C2f2jDRRW6JyUArVIpQ==";
  const EIFFEL_TOWER = { lat: 48.8619, lng: 2.2945 };
  const BIG_BEN = { lat: 51.5021, lng: -0.1242 };
  const COLISEUM = { lat: 41.8961, lng: 12.4879 };

  export default {
    components: {
      GMap
    },
    computed: {
      apiKey: () => API_KEY,
      styleArray: () => styleArray
    },
    data() {
      return {
        location: { lat: EIFFEL_TOWER.lat, lng: EIFFEL_TOWER.lng },
        markers: [
          {
            ...EIFFEL_TOWER,
            onClick: () => console.log("Eiffel Tower")
          },
          {
            ...BIG_BEN,
            onClick: () => console.log("Big Ben")
          },
          {
            ...COLISEUM,
            onClick: () => console.log("Coliseum")
          }
        ]
      };
    }
  };
</script>
```

### Important

You need to create a account on [cloud.google.com](https://console.cloud.google.com), and generate an `API_KEY`, to use **Google Maps** API (and so this component).

## Local Development

If you want more,

You can clone the project:

For **React**:

```
git clone https://github.com/dmnchzl/minimalgooglemapreact.git
```

For **Vue**:

```
git clone https://github.com/dmnchzl/minimalgooglemapvue.git
```

Install dependencies:

```
yarn install
```

Develop locally:

```
yarn dev
```

Run all unit tests:

```
yarn test
```

And finally compile the project:

```
yarn build
```

Enjoy 👍

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
