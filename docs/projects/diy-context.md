---
title: "[DIY] State Management - Solid 🌀"
releaseDate: 2022-07-22

prev:
  text: "Mocca Protocol"
  link: "/projects/mocca-protocol"
next:
  text: "Fibonacci Card 🃏"
  link: "/projects/fibonacci-card"
---

# {{ $frontmatter.title }} <Badge type="tip">solidjs</Badge><Badge type="tip">javascript</Badge>

> [DIY-Context](https://diy-context.vercel.app)

State Management is a reactive programming approach. In the Svelte ecosystem, this concept is associated to Context...  
So, you can do it yourself!

## Process

Repository:

```
git clone https://gitlab.com/dmnchzl/diy-context.git
```

Install:

```
yarn install
```

Dev:

```
yarn dev
```

Build:

```
yarn build
```

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
