---
title: "Translate"
releaseDate: 2025-01-25

prev:
  text: "Projects"
  link: "/projects"
next:
  text: "Mocca (S)CSS - Unopinionated x Utility First CSS Library"
  link: "/projects/mocca-css"
---

# {{ $frontmatter.title }} <Badge type="tip">preact</Badge><Badge type="tip">tailwind</Badge><Badge type="tip">testing</Badge><Badge type="tip">typescript</Badge>

![Translate](/images/translate.webp)

Born from a "_developer's delirium_", [Translate](https://translate.dmnchzl.dev) is an exact copy (I mean graphically) of **Google Translate**, with the difference that this application converts (not languages but):

- Strings in binary, base64, morse, etc.
- Numerical values in octal, hexadecimal, etc.
- Dates into timestamp (and vice versa);
- As well as colors in shades of saturation, brightness, whiteness and blackness.

This app is powered by [Preact](https://preactjs.com/) and [TailwindCSS](https://tailwindcss.com/).

[Playwright](https://playwright.dev/) Guaranteed Testing.

## Process

Repository:

```bash
git clone https://github.com/DmnChzl/Translate.git
```

Install:

```bash
pnpm install
```

Dev:

```bash
pnpm run dev
```

Unit Testing:

```bash
pnpm run test
```

E2E Testing:

```bash
pnpm dlx playwright install
pnpm run test:e2e
```

Build:

```bash
pnpm run build
```

Preview:

```bash
pnpm run preview
```

## License

```
Copyright (c) 2025 Damien Chazoule

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```
