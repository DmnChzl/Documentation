---
title: "Mocca Protocol"
releaseDate: 2022-08-22

prev:
  text: "DatePicker"
  link: "/projects/datepicker"
next:
  text: "[DIY] State Management - Solid 🌀"
  link: "/projects/diy-context"
---

# {{ $frontmatter.title }} <Badge type="tip">deno</Badge><Badge type="tip">solidjs</Badge><Badge type="tip">websocket</Badge>

This is a **FullStack** project for sharing files between users in a **P2P** network. Transaction history is kept in a **BlockChain**. This project is built using these technologies:

- **Deno**: A modern runtime for JavaScript and TypeScript
  - [https://deno.land/](https://deno.land/)
- **SolidJS**: Simple and performant reactivity for building user interfaces
  - [https://www.solidjs.com/](https://www.solidjs.com/)

## Process

Clone the project:

```
git clone https://github.com/DmnChzl/MoccaProtocol.git
```

### Parts

| **Application**                                                                  | **Network**                                                                      |
| -------------------------------------------------------------------------------- | -------------------------------------------------------------------------------- |
| [Go To ReadMe](https://github.com/DmnChzl/MoccaProtocol/blob/main/app/README.md) | [Go To ReadMe](https://github.com/DmnChzl/MoccaProtocol/blob/main/net/README.md) |

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
