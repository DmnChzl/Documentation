---
title: "Mocca (S)CSS - Unopinionated x Utility First CSS Library"
releaseDate: 2024-03-03

prev:
  text: "Translate"
  link: "/translate"
next:
  text: "Staafe - Stay Safe"
  link: "/projects/stay-safe"
---

# {{ $frontmatter.title }} <Badge type="tip">css</Badge>

> _Hi, my name is, what? My name is, who? My name is, "ekekekkekkek", Mocca (S)CSS_

Strongly influenced by [UnoCSS](https://unocss.dev) and [TailwindCSS](https://tailwindcss.com) before that, **Mocca (S)CSS** is a library with no (or little) visual identity, which doesn't provide any out-of-the-box components; just a ton of atomic classes to focus on the template and business logic, rather than the style....

**Mocca (S)CSS** doesn't claim to be lighter (even if it is 😼) or more efficient than TailwindCSS etc.... It's just different, or simply a possibility of what you can do by yourself!

According to [Brad Frost](https://atomicdesign.bradfrost.com)'s atomic model, the scope of **Mocca (S)CSS** covers the "Atoms" part and suggests a standard/convention of what "Ions" or "Design Tokens" should be.

**Mocca (S)CSS** has a fine-grained package to import all or part of the utilities, as needed:

```jsx
import React from "react";
import ReactDOM from "react-dom/client";
import "@dmnchzl/mocca-css/base"; /* Reset Browser Styles */

import "@dmnchzl/mocca-css/colors";
import "@dmnchzl/mocca-css/flex";
import "@dmnchzl/mocca-css/fonts";
import "@dmnchzl/mocca-css/sizing";
import "@dmnchzl/mocca-css/spacing";

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <div className="flex-column p-4">
    <div className="flex-row justify-around gap-x-4">
      <div className="bg-teal-400 radius-2 h-48 w-32">...</div>
      <div className="sqrt-40 bg-indigo-500 radius-4 font-mono">...</div>
      <div className="bg-rose-600 radius-8 h-32 w-48">...</div>
    </div>
  </div>
);
```

## Okay, then...

**Mocca (S)CSS** is a project that combines several really cool web technologies, such as Vite (through [VitePress](https://vitepress.dev)), GitHub Actions and Netlify (CI/CD), but especially SASS (the classic) as well as its `mixins`, `functions` and other loops that remind us how awesome CSS can be to manipulate ❤️

> _**NB**: I also tried adding [LightningCSS](https://lightningcss.dev) for this project; although it seems to be more efficient, I'm currently having issues parsing the SCSS code... So, it's better to stay on solid ground (for the moment)._

## Why "Mocca"?

Mocca! That's simply a reference to my cat 🐱 To learn more about this see the [Object Fit](https://mocca-css.netlify.app/utilities/layout/object-fit) section of the documentation.

## What's next!?

I'm already working on a **TailwindCSS**-style mechanism to play with CSS classes on the fly from HTML code. The idea is to control "_hover_", "_focus_" or "_disabled_" selectors quickly, and also to react to a "_breakpoint_" change on the fly, without using JavaScript code!

Stay Tuned 📣
