---
title: "Fibonacci Card 🃏"
releaseDate: 2021-09-23

prev:
  text: "[DIY] State Management - Solid 🌀"
  link: "/projects/diy-context"
next:
  text: "Minimal Google Map"
  link: "/projects/mnml-gmap"
---

# {{ $frontmatter.title }} <Badge type="tip">svelte</Badge><Badge type="tip">javascript</Badge>

> 1.1.1

I made a simple and colorful application (#ForTheLulz) based on the **Fibonnaci** sequence, for all your user stories (random) evaluation in a **Scrum** process.

This app is powered by [SvelteJS](https://svelte.dev/).

## Preview

![Cards](/images/cards.webp)

## Process

Repository:

```text
git clone https://github.com/DmnChzl/FibonacciCard.git
```

Install:

```text
yarn install
```

Dev:

```text
yarn dev
```

Build:

```text
yarn build
```

Enjoy! 👌

### Tips

Try to reduce the **Fibonnaci** sequence using `urlSearchParam` :

- [fibonacci-card.vercel.app/?limit=55](https://fibonacci-card.vercel.app/?limit=55)

Try the _script_ mode using `urlSearchParam` :

- [fibonacci-card.vercel.app/?mode=script](https://fibonacci-card.vercel.app/?mode=script)

## License

```text
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
