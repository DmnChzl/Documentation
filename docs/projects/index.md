---
layout: doc

next:
  text: "Translate"
  link: "/projects/translate"
---

# Projects

## Translate <Badge type="info">21/01/2025</Badge>

Born from a "_developer's delirium_", [Translate](https://translate.dmnchzl.dev) is an exact copy (I mean graphically) of **Google Translate**, with the difference that this application converts (not languages but):

- Strings in binary, base64, morse, etc.
- Numerical values in octal, hexadecimal, etc.
- Dates into timestamp (and vice versa);
- As well as colors in shades of saturation, brightness, whiteness and blackness.

[Read more...](/projects/translate)

## Mocca (S)CSS - Unopinionated x Utility First CSS Library <Badge type="info">03/03/2024</Badge>

> _Hi, my name is, what? My name is, who? My name is, "ekekekkekkek", Mocca (S)CSS_

Strongly influenced by [UnoCSS](https://unocss.dev) and [TailwindCSS](https://tailwindcss.com) before that, **Mocca (S)CSS** is a library with no (or little) visual identity, which doesn't provide any out-of-the-box components; just a ton of atomic classes to focus on the template and business logic, rather than the style....

[Read more...](/projects/mocca-css)

## Staafe - Stay Safe <Badge type="info">31/03/2023</Badge>

Search the Web quickly, directly from opening a new tab. Based on the **DuckDuckGo** and **Ecosia** search engines, and their "_strict_" mode.

[Read more...](/projects/stay-safe)

## DatePicker <Badge type="info">25/11/2022</Badge>

**[@dmnchzl/datepicker](https://www.npmjs.com/package/@dmnchzl/datepicker/)** is an accessible and customizable `<DatePicker />` component built with **[React](https://reactjs.org)** and other cool stuff (like [styled-components](https://github.com/styled-components/styled-components) or [polished](https://github.com/styled-components/polished))

[Read more...](/projects/datepicker)

## Mocca Protocol <Badge type="info">22/08/2022</Badge>

This is a **FullStack** project for sharing files between users in a **P2P** network. Transaction history is kept in a **BlockChain**. This project is built using these technologies:

- **Deno**: A modern runtime for JavaScript and TypeScript
  - [https://deno.land/](https://deno.land/)
- **SolidJS**: Simple and performant reactivity for building user interfaces
  - [https://www.solidjs.com/](https://www.solidjs.com/)

[Read more...](/projects/mocca-protocol)

## \[DIY\] State Management - Solid 🌀 <Badge type="info">22/07/2022</Badge>

> [DIY-Context](https://diy-context.vercel.app)

State Management is a reactive programming approach. In the Svelte ecosystem, this concept is associated to Context...  
So, you can do it yourself!

[Read more...](/projects/diy-context)

## Fibonacci Card 🃏 <Badge type="info">23/09/2021</Badge>

> 1.1.1

I made a simple and colorful application (#ForTheLulz) based on the **Fibonnaci** sequence, for all your user stories (random) evaluation in a **Scrum** process.

This app is powered by [SvelteJS](https://svelte.dev/).

[Read more...](/projects/fibonacci-card)

## Minimal Google Map <Badge type="info">15/05/2021</Badge>

**[@dmnchzl/mnml-gmap-react](https://www.npmjs.com/package/@dmnchzl/mnml-gmap-react/)** and **[@dmnchzl/mnml-gmap-vue](https://www.npmjs.com/package/@dmnchzl/mnml-gmap-vue/)** are components to simply inject [Google Maps](https://maps.google.com) 🌍 inside your projet.

These libraries are published under the Beerware license, which means you can do whatever you want with the code.

These projects are powered by [ViteJS](https://vitejs.dev).

[Read more...](/projects/mnml-gmap)

## \[DIY\] State Management - Svelte 🌀 <Badge type="info">03/05/2021</Badge>

> [DIY-Stores](https://diy-stores.vercel.app)

State Management is a reactive programming approach. In the Svelte ecosystem, this concept is associated to Stores...  
So, you can do it yourself!

[Read more...](/projects/diy-stores)

## Deno x Alosaur 🦕🦖 <Badge type="info">24/05/2020</Badge>

I made **REST** APIs based on my own discography (and my musical tastes), with these technologies:

- **Deno**: A Secure Runtime For JavaScript And TypeScript
  - [http://deno.land/](http://deno.land/)
- **Alosaur**: Deno Web Framework With Many Decorators
  - [https://github.com/alosaur/](https://github.com/alosaur/alosaur/)

[Read more...](/projects/deno-alosaur)

## Awesome Hooks <Badge type="info">13/02/2020</Badge>

**[@dmnchzl/awesome-hooks](https://www.npmjs.com/package/@dmnchzl/awesome-hooks/)** is a collection of custom React hooks.

This library is published with the Beerware license, which means you can do whatever you want with the code.

[Read more...](/projects/awesome-hooks)

## Reminders 🔔 <Badge type="info">19/06/2019</Badge>

> Special thanks to [@JSaulou](https://github.com/jsaulou) for the inspiration.
> _I owe you one dude !_

**Reminders** is a VS Code extension that allows you to create callbacks directly from the IDE.

[Read more...](/projects/reminders)

## PokeQL <Badge type="info">14/05/2019</Badge>

I made a **Pokédex** app (in French) based on my own progression in the **Pokémon** Go game, with these technologies:

- **ExpressJS**: Fast, unopinionated, minimalist Web Framework for NodeJS
  - [http://expressjs.com/](http://expressjs.com/)
- **Mongoose**: Elegant MongoDB object modeling for NodeJS
  - [https://mongoosejs.com/](https://mongoosejs.com/)
- **GraphQL**: A query language for your API
  - [https://graphql.org/](https://graphql.org/)
- **TypeScript**: TypeScript is a typed superset of JavaScript that compiles to plain JavaScript
  - [https://www.typescriptlang.org/](https://www.typescriptlang.org/)
- **VueJS**: The Progressive JavaScript Framework
  - [https://vuejs.org/](https://vuejs.org/)
- **Apollo**: Replace many inflexible APIs with a single versatile query system
  - [https://www.apollographql.com/](https://www.apollographql.com/)

[Read more...](/projects/pokeql)

## MDWrapper <Badge type="info">01/11/2018</Badge>

**MDWrapper** is a Material Design components library for React, based on Google MDC.

This library is published with the Beerware license, which means you can do whatever you want with the code.

[Read more...](/projects/mdwrapper)
