---
title: PokeQL
releaseDate: 2019-05-14

prev:
  text: Awesome Hooks"
  link: "/projects/awesome-hooks"
next:
  text: "MDWrapper"
  link: "/projects/mdwrapper"
---

# {{ $frontmatter.title }} <Badge type="tip">graphql</Badge><Badge type="tip">vue</Badge><Badge type="tip">typescript</Badge>

I made a **Pokédex** app (in French) based on my own progression in the **Pokémon** Go game, with these technologies:

- **ExpressJS**: Fast, unopinionated, minimalist Web Framework for NodeJS
  - [http://expressjs.com/](http://expressjs.com/)
- **Mongoose**: Elegant MongoDB object modeling for NodeJS
  - [https://mongoosejs.com/](https://mongoosejs.com/)
- **GraphQL**: A query language for your API
  - [https://graphql.org/](https://graphql.org/)
- **TypeScript**: TypeScript is a typed superset of JavaScript that compiles to plain JavaScript
  - [https://www.typescriptlang.org/](https://www.typescriptlang.org/)
- **VueJS**: The Progressive JavaScript Framework
  - [https://vuejs.org/](https://vuejs.org/)
- **Apollo**: Replace many inflexible APIs with a single versatile query system
  - [https://www.apollographql.com/](https://www.apollographql.com/)

## Process

Clone the project:

```
git clone https://github.com/dmnchzl/pokeql.git
```

### Parts

| **Front-End**                                                                 | **Back-End**                                                                 |
| ----------------------------------------------------------------------------- | ---------------------------------------------------------------------------- |
| [Go To ReadMe](https://github.com/DmnChzl/PokeQL/blob/master/Front/README.md) | [Go To ReadMe](https://github.com/DmnChzl/PokeQL/blob/master/Back/README.md) |

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
