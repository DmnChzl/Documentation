---
title: MDWrapper
releaseDate: 2018-11-01

prev:
  text: "PokeQL"
  link: "/projects/pokeql"
---

# {{ $frontmatter.title }} <Badge type="tip">materialdesign</Badge><Badge type="tip">react</Badge><Badge type="tip">npm</Badge>

**MDWrapper** is a Material Design components library for React, based on Google MDC.

This library is published with the Beerware license, which means you can do whatever you want with the code.

## Components

Below, the list of all available components:

| **Component**                                                                                                                                                                                                                                                                                                                                                                                                  | **Source**                                                                                            |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------- |
| [Button](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/button/README.md)                                                                                                                                                                                                                                                                                                                | [Buttons](https://material.io/develop/web/components/buttons)                                         |
| [Card](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/card/README.md) [CardActions](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/card/README.md) [CardActionButtons](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/card/README.md) [CardActionIcons](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/card/README.md) | [Cards](https://material.io/develop/web/components/cards)                                             |
| [CheckBox](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/checkbox/README.md)                                                                                                                                                                                                                                                                                                            | [Checkboxes](https://material.io/develop/web/components/input-controls/checkboxes)                    |
| [ChipSet](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/chipset/README.md) [Chip](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/chipset/README.md)                                                                                                                                                                                                               | [Chips](https://material.io/develop/web/components/chips)                                             |
| [Dialog](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/dialog/README.md)                                                                                                                                                                                                                                                                                                                | [Dialogs](https://material.io/develop/web/components/dialogs)                                         |
| [FAB](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/fab/README.md)                                                                                                                                                                                                                                                                                                                      | [Floating Action Buttons](https://material.io/develop/web/components/buttons/floating-action-buttons) |
| [Icon](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/icon/README.md)                                                                                                                                                                                                                                                                                                                    | [Icons](https://material.io/tools/icons)                                                              |
| [IconButton](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/icon_button/README.md)                                                                                                                                                                                                                                                                                                       | [Icon Buttons](https://material.io/develop/web/components/buttons/icon-buttons)                       |
| [IconToggle](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/icon_toggle/README.md)                                                                                                                                                                                                                                                                                                       | [Icon Toggle Buttons](https://material.io/develop/web/components/buttons/icon-toggle-buttons)         |
| [Grid](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/layout/README.md) [GridInner](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/layout/README.md) [GridCell](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/layout/README.md)                                                                                                             | [Layout Grid](https://material.io/develop/web/components/layout-grid)                                 |
| [LinearProgress](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/linear_progress/README.md)                                                                                                                                                                                                                                                                                               | [Linear Progress](https://material.io/develop/web/components/linear-progress)                         |
| [List](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/list/README.md)                                                                                                                                                                                                                                                                                                                    | [Lists](https://material.io/develop/web/components/lists)                                             |
| [Menu](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/menu/README.md)                                                                                                                                                                                                                                                                                                                    | [Menus](https://material.io/develop/web/components/menus)                                             |
| [Radio](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/radio/README.md)                                                                                                                                                                                                                                                                                                                  | [Radio Buttons](https://material.io/develop/web/components/input-controls/radio-buttons)              |
| [SelectMenu](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/select_menu/README.md)                                                                                                                                                                                                                                                                                                       | [Select Menus](https://material.io/develop/web/components/input-controls/select-menus)                |
| [Slider](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/slider/README.md)                                                                                                                                                                                                                                                                                                                | [Sliders](https://material.io/develop/web/components/input-controls/sliders)                          |
| [Snackbar](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/snackbar/README.md)                                                                                                                                                                                                                                                                                                            | [Snackbars](https://material.io/develop/web/components/snackbars)                                     |
| [Switch](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/switch/README.md)                                                                                                                                                                                                                                                                                                                | [Switches](https://material.io/develop/web/components/input-controls/switches)                        |
| [TextField](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/textfield/README.md)                                                                                                                                                                                                                                                                                                          | [Text Field](https://material.io/develop/web/components/input-controls/text-field)                    |
| [ThemeProvider](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/theme_provider/README.md)                                                                                                                                                                                                                                                                                                 | [Theme](https://material.io/develop/web/components/theme)                                             |
| [TopBar](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/topbar/README.md)                                                                                                                                                                                                                                                                                                                | [Top App Bar](https://material.io/develop/web/components/top-app-bar)                                 |
| [Typography](https://github.com/DmnChzl/MDWrapper/blob/master/src/components/demo/typography/README.md)                                                                                                                                                                                                                                                                                                        | [Typography](https://material.io/develop/web/components/typography)                                   |

### Miscellaneous

If you want more,

You can clone the project:

```
git clone https://github.com/DmnChzl/mdwrapper.git
```

Install dependencies:

```
npm install
```

And launch the project as a web app to see examples:

```
npm run start
```

You can also run all unit tests:

```
npm run test
```

And finally compile the project:

```
npm run build
```

## License

```html
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG>
  wrote this file. As long as you retain this notice you can do whatever you want with this stuff. If we meet some day,
  and you think this stuff is worth it, you can buy me a beer in return. Damien Chazoule</phk@FreeBSD.ORG
>
```
