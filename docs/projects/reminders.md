---
title: "Reminders 🔔"
releaseDate: 2019-06-19

prev:
  text: "Deno x Alosaur 🦕🦖"
  link: "/projects/deno-alosaur"
next:
  text: "PokeQL"
  link: "/projects/pokeql"
---

# {{ $frontmatter.title }} <Badge type="tip">vscode</Badge><Badge type="tip">typescript</Badge>

> Special thanks to [@JSaulou](https://github.com/jsaulou) for the inspiration.
> _I owe you one dude !_

**Reminders** is a VS Code extension that allows you to create callbacks directly from the IDE.

## Usage

1. Install the extension [here](https://marketplace.visualstudio.com/items?itemName=dmnchzl.reminders) ;
2. Add a new reminder (see _Key Bindings_ section) ;
3. Show all reminders (by clicking on the status bar item) ;
4. Wait... If your reminder contains the pattern `12am`, `6pm`, `20:45` or `8h15`, it will be displayed in due time !
5. Also you can clear all reminders, or close and re-open your IDE for wiping the list.

**Enjoy !**

### Extension Settings

- `reminders.list` : List of reminders
- `reminders.preserve` : Optional parameter that keeps the list of reminders
- `reminders.shift` : Optional parameter to display the reminder before it's due time

### Key Bindings

- `Shift + Alt + R` : Add a new reminder

### Supported Languages

- [x] English
- [x] French
- [x] Spanish

## [Change Log](https://github.com/DmnChzl/Reminders/blob/master/CHANGELOG.md)

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
