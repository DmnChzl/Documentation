---
title: "[DIY] State Management - Svelte 🌀"
releaseDate: 2021-03-05

prev:
  text: "Minimal Google Map"
  link: "/projects/mnml-gmap"
next:
  text: "Deno x Alosaur 🦕🦖"
  link: "/projects/deno-alosaur"
---

# {{ $frontmatter.title }} <Badge type="tip">svelte</Badge><Badge type="tip">javascript</Badge>

> [DIY-Stores](https://diy-stores.vercel.app)

State Management is a reactive programming approach. In the Svelte ecosystem, this concept is associated to Stores...  
So, you can do it yourself!

## Process

Repository:

```
git clone https://gitlab.com/dmnchzl/diy-stores.git
```

Install:

```
yarn install
```

Dev:

```
yarn dev
```

Build:

```
yarn build
```

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
