---
title: "[DIY] State Management - Svelte 🌀"
releaseDate: 2021-03-05

prev:
  text: "Minimal Google Map"
  link: "/fr/projects/mnml-gmap"
next:
  text: "Deno x Alosaur 🦕🦖"
  link: "/fr/projects/deno-alosaur"
---

# {{ $frontmatter.title }} <Badge type="tip">svelte</Badge><Badge type="tip">javascript</Badge>

> [DIY-Stores](https://diy-stores.vercel.app)

Le State Management est une approche de programmation réactive. Dans l'écosystème Svelte, ce concept est associé aux Stores...
Alors, vous pouvez le faire vous-même !

## Processus

Dépot:

```
git clone https://gitlab.com/dmnchzl/diy-stores.git
```

Installation:

```
yarn install
```

Développement:

```
yarn dev
```

Compilation:

```
yarn build
```

## Licence

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
