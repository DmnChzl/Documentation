---
title: "Staafe - Stay Safe"
releaseDate: 2023-03-31

prev:
  text: "Mocca (S)CSS - Unopinionated x Utility First CSS Library"
  link: "/fr/projects/mocca-css"
next:
  text: "DatePicker"
  link: "/fr/projects/datepicker"
---

# {{ $frontmatter.title }} <Badge type="tip">preact</Badge><Badge type="tip">chrome</Badge><Badge type="tip">firefox</Badge><Badge type="tip">typescript</Badge>

Rechercher rapidement sur le Web, directement depuis l'ouverture d'un nouvel onglet. Basé sur les moteurs de recherche **DuckDuckGo** et **Ecosia**, et leur mode "_stricte_".

## Aperçu

![Light](https://raw.githubusercontent.com/dmnchzl/staafe/main/screenshots/light.png)
![Dark](https://raw.githubusercontent.com/dmnchzl/staafe/main/screenshots/dark.png)

## Comment Installer

### Firefox

1. Accéder à `about:config`
2. Basculer `xpinstall.signatures.required` sur **false**
3. Accéder à `about:addons`
4. Installer un module depuis un fichier

### Chrome

1. Accéder à `chrome://extensions`
2. Activer le mode développeur
3. Charger l'extension non empaquetée

## Divers

Si vous en voulez plus,

Vous pouvez cloner le projet :

```
git clone https://github.com/dmnchzl/staafe.git
```

Installer les dépendances :

```
yarn install
```

Développer en local :

```
yarn dev
```

Si vous le souhaitez, formater le code :

```
yarn format
```

Compiler le projet :

```
yarn build
```

compiler le projet (en tant qu'extension) :

```
yarn build:ext --overwrite-dest
```

Enjoy 👍

## Licence

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
