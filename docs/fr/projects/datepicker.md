---
title: DatePicker
releaseDate: 2022-11-25

prev:
  text: "Staafe - Stay Safe"
  link: "/fr/projects/stay-safe"
next:
  text: "Mocca Protocol"
  link: "/fr/projects/mocca-protocol"
---

# {{ $frontmatter.title }} <Badge type="tip">react</Badge><Badge type="tip">typescript</Badge><Badge type="tip">css</Badge>

**[@dmnchzl/datepicker](https://www.npmjs.com/package/@dmnchzl/datepicker/)** est un composant `<DatePicker />` accessible et personnalisable construit avec **[React](https://reactjs.org)** et d'autres trucs cool (comme [styled-components](https://github.com/styled-components/styled-components) ou [polished](https://github.com/styled-components/polished))

## Propriétés

Ci-dessous, la liste de toutes les propriétés disponibles :

**color** / **bgColor**

> Colorer le texte / l'arrière plan du composant

Par défaut, les couleurs du composant sont respectivement violet (`#673ab8`) et blanc (`#fff`) pour l'arrière plan

```jsx
import { DatePicker } from "@dmnchzl/awesome-hooks";

export default function App() {
  return (
    <div>
      <DatePicker color="#ffc107" bgColor="#171717">
        {props => (
          <input
            ref={props.ref}
            type="date"
            onClick={e => {
              e.preventDefault();
              props.onOpen();
            }}
          />
        )}
      </DatePicker>
    </div>
  );
}
```

**dayOfTheWeek**

> Changer le jour de début de la semaine

Dimanche (`0`) est le jour de début de semaine par défaut, mais les Français préfèrent commencer la semaine le Lundi (`1`)

```jsx
import { DatePicker } from "@dmnchzl/awesome-hooks";

export default function App() {
  return (
    <div>
      <DatePicker dayOfTheWeek={1}>
        {props => (
          <input
            ref={props.ref}
            type="date"
            onClick={e => {
              e.preventDefault();
              props.onOpen();
            }}
          />
        )}
      </DatePicker>
    </div>
  );
}
```

**forceLocale**

> Forcer la locale / la langue

Par défaut, le composant est basé sur la langue du navigateur, mais vous pouvez forcer la locale afin d'avoir plusieurs `<DatePicker />` différents sur la même vue

_**NB** : Actuellement, le composant prend en charge les locales suivantes : `de` / `en` / `es` / `fr` / `it` / `ja` ; pour aller plus loin, jetez un coup d'oeil à la propriété `customMessages` ci-dessous..._

```jsx
import { DatePicker } from "@dmnchzl/awesome-hooks";

export default function App() {
  return (
    <div>
      <DatePicker forceLocale="ja">
        {props => (
          <input
            ref={props.ref}
            type="date"
            onClick={e => {
              e.preventDefault();
              props.onOpen();
            }}
          />
        )}
      </DatePicker>
    </div>
  );
}
```

**hideFooter**

> Masquer le pied de page du composant (et ses boutons)

Lorsque le pied de page est masqué, le calendrier disparaît lorsqu'une date est sélectionnée / ou lorsque le bouton d'échappement est pressé\_

```jsx
import { DatePicker } from "@dmnchzl/awesome-hooks";

export default function App() {
  return (
    <div>
      <DatePicker hideFooter>
        {props => (
          <input
            ref={props.ref}
            type="date"
            onClick={e => {
              e.preventDefault();
              props.onOpen();
            }}
          />
        )}
      </DatePicker>
    </div>
  );
}
```

**customMessages**

> (Re)traduire tout ou partie du composant

```jsx
import { DatePicker } from "@dmnchzl/awesome-hooks";

export default function App() {
  return (
    <div>
      <DatePicker customMessages={{ "Footer.Cancel": "Nope", "Footer.Ok": "Okay" }}>
        {props => (
          <input
            ref={props.ref}
            type="date"
            onClick={e => {
              e.preventDefault();
              props.onOpen();
            }}
          />
        )}
      </DatePicker>
    </div>
  );
}
```

| Month Keys        | Days Key        | Footer Keys     |
| ----------------- | --------------- | --------------- |
| `Month.January`   | `Day.Sunday`    | `Footer.Today`  |
| `Month.February`  | `Day.Monday`    | `Footer.Cancel` |
| `Month.March`     | `Day.Tuesday`   | `Footer.Ok`     |
| `Month.April`     | `Day.Wednesday` |                 |
| `Month.March`     | `Day.Thursday`  |                 |
| `Month.May`       | `Day.Friday`    |                 |
| `Month.June`      | `Day.Saturday`  |                 |
| `Month.July`      |                 |                 |
| `Month.August`    |                 |                 |
| `Month.September` |                 |                 |
| `Month.October`   |                 |                 |
| `Month.November`  |                 |                 |
| `Month.December`  |                 |                 |

## Aperçu

![Purple White](https://raw.githubusercontent.com/DmnChzl/DatePicker/main/preview/purpleWhite.png)
![Pink Shrine](https://raw.githubusercontent.com/DmnChzl/DatePicker/main/preview/pinkShrine.png)
![Green Basil](https://raw.githubusercontent.com/DmnChzl/DatePicker/main/preview/greenBasil.png)
![Amber Dark](https://raw.githubusercontent.com/DmnChzl/DatePicker/main/preview/amberDark.png)

## Structure Projet

```
/
├── src/
│   ├── components/
│   │   ├── __tests__/
│   │   │   └── DatePicker.test.tsx
│   │   ├── icons/
│   │   │   ├── ChevronLeft.tsx
│   │   │   ├── ChevronRight.tsx
│   │   │   ├── DoubleChevronLeft.tsx
│   │   │   ├── DoubleChevronRight.tsx
│   │   │   └── index.ts
│   │   ├── DatePicker.tsx
│   │   └── styled.ts
│   ├── constants/
│   │   └── index.ts
│   └── utils/
│       ├── __tests__/
│       │   ├── arrUtils.test.ts
│       │   └── dateUtils.test.ts
│       ├── arrUtils.ts
│       └── dateUtils.ts
├── package.json
└── vite.config.ts
```

## Divers

Si vous en voulez plus,

Vous pouvez cloner le projet :

```
git clone https://github.com/dmnchzl/datepicker.git
```

Installer les dépendances :

```
yarn install
```

Développer en local :

```
yarn dev
```

Exécuter tous les tests unitaires :

```
yarn test
```

Si vous le souhaitez, formater le code :

```
yarn format
```

Et enfin compiler le projet :

```
yarn build
```

Enjoy 👍

## Licence

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
