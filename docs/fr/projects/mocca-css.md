---
title: "Mocca (S)CSS - Unopinionated x Utility First CSS Library"
releaseDate: 2024-03-03

prev:
  text: "Translate"
  link: "/fr/translate"
next:
  text: "Staafe - Stay Safe"
  link: "/fr/projects/stay-safe"
---

# {{ $frontmatter.title }} <Badge type="tip">css</Badge>

Fortement influencée par [UnoCSS](https://unocss.dev) et [TailwindCSS](https://tailwindcss.com) avant cela, [Mocca (S)CSS](https://mocca-css.netlify.app) est une bibliothèque sans (ou peu) d'identité visuelle, qui ne fournit aucun composant prêt à l'emploi ; juste une tonne de classes atomiques pour se concentrer sur le template et la logique métier, plutôt que sur le style....

**Mocca (S)CSS** ne prétend pas être plus léger (même si c'est le cas 😼) ou plus efficace que TailwindCSS etc.... C'est juste différent, ou simplement une possibilité de ce que vous pouvez faire par vous-même !

D'après le modèle atomique de [Brad Frost](https://atomicdesign.bradfrost.com), la portée de **Mocca (S)CSS** couvre la partie "Atoms" et suggère une norme/convention sur ce que devraient être les "Ions" ou les "Design Tokens".

**Mocca (S)CSS** possède un package à granularité fine pour importer tout ou partie des utilitaires, selon les besoins :

```jsx
import React from "react";
import ReactDOM from "react-dom/client";
import "@dmnchzl/mocca-css/base"; /* Reset Browser Styles */

import "@dmnchzl/mocca-css/colors";
import "@dmnchzl/mocca-css/flex";
import "@dmnchzl/mocca-css/fonts";
import "@dmnchzl/mocca-css/sizing";
import "@dmnchzl/mocca-css/spacing";

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <div className="flex-column p-4">
    <div className="flex-row justify-around gap-x-4">
      <div className="bg-teal-400 radius-2 h-48 w-32">...</div>
      <div className="sqrt-40 bg-indigo-500 radius-4 font-mono">...</div>
      <div className="bg-rose-600 radius-8 h-32 w-48">...</div>
    </div>
  </div>
);
```

## Ok, mais encore...

**Mocca (S)CSS** c'est un projet qui combine plusieurs technologies Web vraiment cool, tel que Vite (au travers de [VitePress](https://vitepress.dev)), GitHub Actions et Netlify (CI/CD), mais surtout SASS (le classique) ainsi que ses `mixins`, `functions` et autres boucles qui nous rappellent à quel point le CSS peut être génial à manipuler ❤️

> _**NB** : J'ai aussi essayé d'ajouter [LightningCSS](https://lightningcss.dev) pour ce projet ; bien qu'il semble être plus performant, j'éprouve actuellement des difficultés à parser le code SCSS... Donc, mieux vaut rester sur des bases solides (pour le moment)._

## Pourquoi "Mocca" ?

Mocca ! C'est simplement une référence à mon chat 🐱 Pour en savoir plus à ce sujet, consulter la section [Object Fit](https://mocca-css.netlify.app/utilities/layout/object-fit) de la documentation.

## C'est quoi la suite !?

Je travaille déjà sur un mécanisme à-la-**TailwindCSS** pour jouer avec les classes CSS à la volée depuis le code HTML. L'idée étant de contrôler les sélecteurs "_hover_", "_focus_" ou encore "_disabled_" rapidement, et également de réagir à un changement de "_breakpoint_" à chaud, sans utiliser de code JavaScript !

Stay Tuned 📣
