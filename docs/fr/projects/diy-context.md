---
title: "[DIY] State Management - Solid 🌀"
releaseDate: 2022-07-22

prev:
  text: "Mocca Protocol"
  link: "/fr/projects/mocca-protocol"
next:
  text: "Fibonacci Card 🃏"
  link: "/fr/projects/fibonacci-card"
---

# {{ $frontmatter.title }} <Badge type="tip">solidjs</Badge><Badge type="tip">javascript</Badge>

> [DIY-Context](https://diy-context.vercel.app)

Le State Management est une approche de programmation réactive. Dans l'écosystème Solid, ce concept est associé au Context...
Alors, vous pouvez le faire vous-même !

## Processus

Dépot:

```
git clone https://gitlab.com/dmnchzl/diy-context.git
```

Installation:

```
yarn install
```

Développement:

```
yarn dev
```

Compilation:

```
yarn build
```

## Licence

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
