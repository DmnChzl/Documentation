---
title: "Mocca Protocol"
releaseDate: 2022-08-22

prev:
  text: "DatePicker"
  link: "/fr/projects/datepicker"
next:
  text: "[DIY] State Management - Solid 🌀"
  link: "/fr/projects/diy-context"
---

# {{ $frontmatter.title }} <Badge type="tip">deno</Badge><Badge type="tip">solidjs</Badge><Badge type="tip">websocket</Badge>

Voici un projet **FullStack** pour le partage de fichiers entre utilisateurs dans un réseau **P2P**. L'historique des transactions est conservé dans une **BlockChain**. Ce projet est construit à l'aide de ces technologies :

- **Deno**: Un runtime moderne pour JavaScript et TypeScript
  - [https://deno.land/](https://deno.land/)
- **SolidJS**: Réactivité simple et performante pour la construction d'interfaces utilisateurs
  - [https://www.solidjs.com/](https://www.solidjs.com/)

## Processus

Cloner le projet :

```
git clone https://github.com/DmnChzl/MoccaProtocol.git
```

### Parties

| **Application**                                                                  | **Network**                                                                      |
| -------------------------------------------------------------------------------- | -------------------------------------------------------------------------------- |
| [Go To ReadMe](https://github.com/DmnChzl/MoccaProtocol/blob/main/app/README.md) | [Go To ReadMe](https://github.com/DmnChzl/MoccaProtocol/blob/main/net/README.md) |

## Licence

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
