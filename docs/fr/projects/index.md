---
layout: doc

next:
  text: "Translate"
  link: "/fr/projects/translate"
---

# Projets

## Translate <Badge type="info">21/01/2025</Badge>

Né d'un "_délire de développeur_", [Translate](https://translate.dmnchzl.dev) est une copie conforme (visuellement parlant) de **Google Translate**, à la différence près que cette application convertie (non pas des langues mais) :

- Des chaînes de caractères en binaire, base64, morse, etc...
- Des valeurs numériques en valeurs octales/hexadécimales, etc...
- Des dates en timestamp (et vice-versa) ;
- Ainsi que des couleurs en nuances de saturation, de luminosité, de blancheur et de noirceur.

[En savoir plus...](/fr/projects/translate)

## Mocca (S)CSS - Unopinionated x Utility First CSS Library <Badge type="info">03/03/2024</Badge>

> _Hi, my name is, what? My name is, who? My name is, "ekekekkekkek", Mocca (S)CSS_

Fortement influencée par [UnoCSS](https://unocss.dev) et [TailwindCSS](https://tailwindcss.com) avant cela, [Mocca (S)CSS](https://mocca-css.netlify.app) est une bibliothèque sans (ou peu) d'identité visuelle, qui ne fournit aucun composant prêt à l'emploi ; juste une tonne de classes atomiques pour se concentrer sur le template et la logique métier, plutôt que sur le style....

[En savoir plus...](/fr/projects/mocca-css)

## Staafe - Stay Safe <Badge type="info">31/03/2023</Badge>

Rechercher rapidement sur le Web, directement depuis l'ouverture d'un nouvel onglet. Basé sur les moteurs de recherche **DuckDuckGo** et **Ecosia**, et leur mode "_stricte_".

[En savoir plus...](/fr/projects/stay-safe)

## DatePicker <Badge type="info">25/11/2022</Badge>

**[@dmnchzl/datepicker](https://www.npmjs.com/package/@dmnchzl/datepicker/)** est un composant `<DatePicker />` accessible et personnalisable construit avec **[React](https://reactjs.org)** et d'autres trucs cool (comme [styled-components](https://github.com/styled-components/styled-components) ou [polished](https://github.com/styled-components/polished))

[En savoir plus...](/fr/projects/datepicker)

## Mocca Protocol <Badge type="info">22/08/2022</Badge>

Voici un projet **FullStack** pour le partage de fichiers entre utilisateurs dans un réseau **P2P**. L'historique des transactions est conservé dans une **BlockChain**. Ce projet est construit à l'aide de ces technologies :

- **Deno**: Un runtime moderne pour JavaScript et TypeScript
  - [https://deno.land/](https://deno.land/)
- **SolidJS**: Réactivité simple et performante pour la construction d'interfaces utilisateurs
  - [https://www.solidjs.com/](https://www.solidjs.com/)

[En savoir plus...](/fr/projects/mocca-protocol)

## \[DIY\] State Management - Solid 🌀 <Badge type="info">22/07/2022</Badge>

> [DIY-Context](https://diy-context.vercel.app)

Le State Management est une approche de programmation réactive. Dans l'écosystème Solid, ce concept est associé au Context...
Alors, vous pouvez le faire vous-même !

[En savoir plus...](/fr/projects/diy-context)

## Fibonacci Card 🃏 <Badge type="info">23/09/2021</Badge>

> 1.1.1

J'ai réalisé une application simple et colorée (#ForTheLulz) basée sur la séquence de **Fibonacci**, pour l'évaluation (aléatoire) de toutes vos user stories dans un processus **Scrum**.

Cette application est propulsée par [SvelteJS](https://svelte.dev/).

[En savoir plus...](/fr/projects/fibonacci-card)

## Minimal Google Map <Badge type="info">15/05/2021</Badge>

**[@dmnchzl/mnml-gmap-react](https://www.npmjs.com/package/@dmnchzl/mnml-gmap-react/)** et **[@dmnchzl/mnml-gmap-vue](https://www.npmjs.com/package/@dmnchzl/mnml-gmap-vue/)** sont des composants pour injecter simplement [Google Maps](https://maps.google.com) 🌍 dans votre projet.

Ces bibliothèques sont publiées sous la licence Beerware, ce qui signifie que vous pouvez faire ce que vous voulez avec le code.

Ces projets sont propulsés par [ViteJS](https://vitejs.dev).

[En savoir plus...](/fr/projects/mnml-gmap)

## \[DIY\] State Management - Svelte 🌀 <Badge type="info">03/05/2021</Badge>

> [DIY-Stores](https://diy-stores.vercel.app)

Le State Management est une approche de programmation réactive. Dans l'écosystème Svelte, ce concept est associé aux Stores...
Alors, vous pouvez le faire vous-même !

[En savoir plus...](/fr/projects/diy-stores)

## Deno x Alosaur 🦕🦖 <Badge type="info">24/05/2020</Badge>

J'ai fait des APIs **REST** basées sur ma propre discographie (et mes goûts musicaux), avec ces technologies:

- **Deno**: Un runtime sécurisé pour JavaScript et TypeScript
  - [http://deno.land/](http://deno.land/)
- **Alosaur**: Framework Web Deno avec de nombreux décorateurs
  - [https://github.com/alosaur/](https://github.com/alosaur/alosaur/)

[En savoir plus...](/fr/projects/deno-alosaur)

## Awesome Hooks <Badge type="info">13/02/2020</Badge>

**[@dmnchzl/awesome-hooks](https://www.npmjs.com/package/@dmnchzl/awesome-hooks/)** est une collection de hooks React personnalisés.

Cette bibliothèque est publiée avec la licence Beerware, ce qui signifie que vous pouvez faire ce que vous voulez avec le code.

[En savoir plus...](/fr/projects/awesome-hooks)

## Reminders 🔔 <Badge type="info">19/06/2019</Badge>

> Remerciement spécial à [@JSaulou](https://github.com/jsaulou) pour l'inspiration.
> _Je t'en dois une mec !_

**Reminders** est une extension VS Code qui vous permet de créer des rappels directement depuis l'IDE.

[En savoir plus...](/fr/projects/reminders)

## PokeQL <Badge type="info">14/05/2019</Badge>

J'ai réalisé une application **Pokédex** basée sur ma propre progression dans le jeu **Pokémon** Go, avec ces technologies :

- **ExpressJS**: Framework Web rapide, dogmatique et minimaliste pour NodeJS
  - [http://expressjs.com/](http://expressjs.com/)
- **Mongoose**: Modélisation d'objets MongoDB élégante pour NodeJS
  - [https://mongoosejs.com/](https://mongoosejs.com/)
- **GraphQL**: Un langage de requête pour votre API
  - [https://graphql.org/](https://graphql.org/)
- **TypeScript**: TypeScript est un sur-ensemble typé de JavaScript qui se compile en JavaScript brut
  - [https://www.typescriptlang.org/](https://www.typescriptlang.org/)
- **VueJS**: Le Framework JavaScript Progressif
  - [https://vuejs.org/](https://vuejs.org/)
- **Apollo**: Remplacez de nombreuses APIs inflexibles par un seul système de requête polyvalent
  - [https://www.apollographql.com/](https://www.apollographql.com/)

[En savoir plus...](/fr/projects/pokeql)

## MDWrapper <Badge type="info">01/11/2018</Badge>

**MDWrapper** est une bibliothèque de composants Material Design pour React, basée sur Google MDC.

Cette bibliothèque est publiée avec la licence Beerware, ce qui signifie que vous pouvez faire ce que vous voulez avec le code.

[En savoir plus...](/fr/projects/mdwrapper)
