---
title: "Awesome Hooks"
releaseDate: 2020-02-13

prev:
  text: "Deno x Alosaur 🦕🦖"
  link: "/fr/projects/deno-alosaur"
next:
  text: "Reminders 🔔"
  link: "/fr/projects/reminders"
---

# {{ $frontmatter.title }} <Badge type="tip">react</Badge><Badge type="tip">npm</Badge>

**[@dmnchzl/awesome-hooks](https://www.npmjs.com/package/@dmnchzl/awesome-hooks/)** est une collection de hooks React personnalisés.

Cette bibliothèque est publiée avec la licence Beerware, ce qui signifie que vous pouvez faire ce que vous voulez avec le code.

## Hooks

Ci-dessous, la liste de tous les hooks disponibles :

**useCounter**

> _Utilisez-le pour créer un compteur ~~simple~~_

```js
import React from "react";
import { useCounter } from "@dmnchzl/awesome-hooks";

export default function Counter(props) {
  const { value, add, del, reset } = useCounter(0);

  return (
    <div>
      {value}
      <button onClick={() => add(2)}>+2</button>
      <button onClick={() => del(2)}>-2</button>
      <button onClick={reset}>0</button>
    </div>
  );
}
```

**useDocumentTitle**

> _Utilisez-le pour changer le titre du site_

```js
import React from "react";
import { useDocumentTitle } from "@dmnchzl/awesome-hooks";

export default function HelloWorld(props) {
  useDocumentTitle("Hello World");

  return <div>{/* ... */}</div>;
}
```

**useMeta**

> _Utilisez-le pour ajouter / modifier une métadonnée_

```js
import React from "react";
import { useMeta } from "@dmnchzl/awesome-hooks";

export default function LoremIpsum(props) {
  useMeta("theme-color", "#2a2c2e");
  useMeta("description", "Lorem Ipsum Dolor Sit Amet");

  return <div>{/* ... */}</div>;
}
```

**useObject**

> _Utilisez-le pour manipuler un objet_

```js
import React, { useEffect } from "react";
import { useObject } from "@dmnchzl/awesome-hooks";

export default function App(props) {
  const [person, setPerson, isEmpty] = useObject({
    firstName: "Morty",
    lastName: "Smith"
  });

  useEffect(() => {
    setPerson({
      firstName: "Rick",
      lastName: "Sanchez"
    });
  }, [setPerson]);

  return (
    <ul>
      {!isEmpty &&
        Object.entries(person).map(([key, value], idx) => (
          <li key={idx}>
            {key}: {value}
          </li>
        ))}
    </ul>
  );
}
```

**useInput**

> _Utilisez-le pour gérer le comportement d'un champ de saisie_

```js
import React from "react";
import { useInput } from "@dmnchzl/awesome-hooks";

export default function Input(props) {
  const [value, setValue] = useInput("");

  return <input defaultValue={value} onChange={setChange} />;
}
```

**useField**

> _Utilisez-le pour associer une valeur à une erreur potentielle (exemple avec un champ de formulaire)_

```js
import React from "react";
import { useField } from "@dmnchzl/awesome-hooks";

export default function Form(props) {
  const { value, error, setValue, setError, reset } = useField("");

  const handleSubmit = event => {
    event.preventDefault();

    if (value.length < 5) {
      setError("Too Short");
    } else {
      setError("Too Long");
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <input defaultValue={value} onChange={e => setValue(e.target.value)} />
      {error && <p>{error}</p>}
      <button type="submit">Submit</button>
      <button onClick={reset}>Reset</button>
    </form>
  );
}
```

**useArray**

> _Utilisez-le pour gérer un tableau_

```js
import React, { useEffect } from 'react';
import { useArray } from '@dmnchzl/awesome-hooks';

export default function List(props) {
  const { values, setValues, addValue, setValue, delValue } = useArray([]);

  useEffect(() => {
    if (values.length < 0) {
      setValues([
        { firstName: 'Rick', lastName: 'Sanchez' },
        { firstName: 'Morty', lastName: 'Smith' }
      ]);
    }
  }, [values, setValues]);

  return (
    <ul>
      {values.map(({ firstName, lastName }) => (
        <li>
          <input defaultValue={value.firstName} onChange={e => setValue(e.target.value, 'firstName')}>
          <button onClick={() => delValue(lastName, 'lastName')}>
            Del
          </button>
        </li>
      ))}
      <button onClick={() => addValue({ firstName: 'Summer', lastName: 'Smith' })}>
        Add
      </button>
    </ul>
  );
}
```

**useMatrix**

> _Utilisez-le pour transformer un tableau en matrice (utile pour se conformer aux normes de bootstrap)_

```js
import React, { useEffect } from "react";
import { useMatrix } from "@dmnchzl/awesome-hooks";

export default function Grid(props) {
  const { rows, setValues } = useMatrix(4);

  useEffect(() => {
    let values = [];

    for (let i = 0; i < 12; i++) {
      values = [...values, `Val ${i}`];
    }

    setValues(values);
  }, [setValues]);

  return (
    <div>
      {rows.map((cols, i) => (
        <div key={i} className="row">
          {cols.map((value, j) => (
            <div key={j} className="col">
              {value}
            </div>
          ))}
        </div>
      ))}
    </div>
  );
}
```

**useTimer**

> _Utilisez-le pour jouer avec une chronomètre_

```js
import React from "react";
import { useTimer } from "@dmnchzl/awesome-hooks";

export default function Calendar(props) {
  const { days, hours, minutes, seconds } = useTimer(2020, 4, 4, 12);

  return (
    <div>
      Remainin' Time Before May The 4th...
      <h1>Days: {days}</h1>
      <h2>Hours: {hours}</h2>
      <h3>Minutes: {minutes}</h3>
      <h4>Seconds: {seconds}</h4>
    </div>
  );
}
```

**useStorage**

> _Utilisez-le pour gérer un objet (et le faire persister dans le session / local storage)_

```js
import React, { useEffect } from "react";
import { useStorage } from "@dmnchzl/awesome-hooks";

const USE_LOCAL_STORAGE = true;

export default function App(props) {
  const [person, setPerson] = useStorage("person", USE_LOCAL_STORAGE);

  useEffect(() => {
    setPerson({
      firstName: "Rick",
      lastName: "Sanchez"
    });
  }, [setPerson]);

  return (
    <ul>
      {Object.entries(person).map(([key, value], idx) => (
        <li key={idx}>
          {key}: {value}
        </li>
      ))}
    </ul>
  );
}
```

### Divers

Si vous en voulez plus,

Vous pouvez cloner le projet :

```
git clone https://github.com/dmnchzl/awesomehooks.git
```

Installer les dépendances :

```
yarn install
```

Exécuter tous les tests unitaires :

```
yarn test
```

Et enfin compiler le projet :

```
yarn build
```

## Licence

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
