---
title: "Minimal Google Map"
releaseDate: 2021-05-15

prev:
  text: "Fibonacci Card 🃏"
  link: "/fr/projects/fibonacci-card"
next:
  text: "[DIY] State Management - Svelte 🌀"
  link: "/fr/projects/diy-stores"
---

# {{ $frontmatter.title }} <Badge type="tip">react</Badge><Badge type="tip">vue</Badge><Badge type="tip">typescript</Badge><Badge type="tip">googlemaps</Badge>

**[@dmnchzl/mnml-gmap-react](https://www.npmjs.com/package/@dmnchzl/mnml-gmap-react/)** et **[@dmnchzl/mnml-gmap-vue](https://www.npmjs.com/package/@dmnchzl/mnml-gmap-vue/)** sont des composants pour injecter simplement [Google Maps](https://maps.google.com) 🌍 dans votre projet.

Ces bibliothèques sont publiées sous la licence Beerware, ce qui signifie que vous pouvez faire ce que vous voulez avec le code.

Ces projets sont propulsés par [ViteJS](https://vitejs.dev).

## Comment Ça Marche

Tout d'abord, vous devez installer le package :

Avec **React**:

```
  npm install @dmnchzl/mnml-gmap-react
```

Avec **Vue**:

```
  npm install @dmnchzl/mnml-gmap-vue
```

Ensuite, utilisez le composant comme ces exemples ci-dessous :

En **React** :

```js
import React from "react";
import { GMap } from "@dmnchzl/mnml-gmap-react";
import styleArray from "./styleArray";

/**
 * The 'keyke' (Key + Fake = Cake) is a lie
 * NB: Worst pun ever; i know that...
 */
const API_KEY = "01wNJmwnZEabSeRBV4vQRV1JMUx1MvyMyA6edTN49PF/yf0aUjY2F4NZfIOq/Yh7x0d38+21sEP6Tck/jEmXdw==";
const EIFFEL_TOWER = { lat: 48.8619, lng: 2.2945 };
const BIG_BEN = { lat: 51.5021, lng: -0.1242 };
const COLISEUM = { lat: 41.8961, lng: 12.4879 };

function App() {
  return (
    <div className="container">
      <GMap
        apiKey={API_KEY}
        defaultCenter={EIFFEL_TOWER}
        defaultZoom={5}
        markers={[
          { ...EIFFEL_TOWER, onClick: () => console.log("Eiffel Tower") },
          { ...BIG_BEN, onClick: () => console.log("Big Ben") },
          { ...COLISEUM, onClick: () => console.log("Coliseum") }
        ]}
        styledMap={styleArray}
        enableUI
      />
    </div>
  );
}
```

En **Vue** :

```html
<template>
  <div class="container">
    <GMap
      :apiKey="apiKey"
      :defaultCenter="location"
      :defaultZoom="5"
      :markers="markers"
      :styledMap="styleArray"
      :enableUI="true" />
  </div>
</template>

<script>
  import { GMap } from "@dmnchzl/mnml-gmap-vue";
  import styleArray from "./styleArray";

  /**
   * The 'keyke' (Key + Fake = Cake) is a lie
   * NB: Worst pun ever; i know that...
   */
  const API_KEY = "YLzo6G7hJxAg55x/EvkEaPTvoO/PhQ9ASxzZ5A6OsNJLE4dxq+zK2lJ5Ta7/z5bbtQ9C2f2jDRRW6JyUArVIpQ==";
  const EIFFEL_TOWER = { lat: 48.8619, lng: 2.2945 };
  const BIG_BEN = { lat: 51.5021, lng: -0.1242 };
  const COLISEUM = { lat: 41.8961, lng: 12.4879 };

  export default {
    components: {
      GMap
    },
    computed: {
      apiKey: () => API_KEY,
      styleArray: () => styleArray
    },
    data() {
      return {
        location: { lat: EIFFEL_TOWER.lat, lng: EIFFEL_TOWER.lng },
        markers: [
          {
            ...EIFFEL_TOWER,
            onClick: () => console.log("Eiffel Tower")
          },
          {
            ...BIG_BEN,
            onClick: () => console.log("Big Ben")
          },
          {
            ...COLISEUM,
            onClick: () => console.log("Coliseum")
          }
        ]
      };
    }
  };
</script>
```

### Important

Vous devez créer un compte sur [cloud.google.com](https://console.cloud.google.com) et générer une `API_KEY` pour utiliser l'API **Google Maps** (et donc ce composant).

## Développement Local

Si vous en voulez plus,

Vous pouvez cloner le projet :

Pour **React**:

```
git clone https://github.com/dmnchzl/minimalgooglemapreact.git
```

Pour **Vue**:

```
git clone https://github.com/dmnchzl/minimalgooglemapvue.git
```

Installer les dépendances :

```
yarn install
```

Développer localement :

```
yarn dev
```

Exécuter tous les tests unitaires :

```
yarn test
```

Et enfin compiler le projet :

```
yarn build
```

Enjoy 👍

## Licence

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
