---
title: "Reminders 🔔"
releaseDate: 2019-06-19

prev:
  text: "Deno x Alosaur 🦕🦖"
  link: "/fr/projects/deno-alosaur"
next:
  text: "PokeQL"
  link: "/fr/projects/pokeql"
---

# {{ $frontmatter.title }} <Badge type="tip">vscode</Badge><Badge type="tip">typescript</Badge>

> Remerciement spécial à [@JSaulou](https://github.com/jsaulou) pour l'inspiration.
> _Je t'en dois une mec !_

**Reminders** est une extension VS Code qui vous permet de créer des rappels directement depuis l'IDE.

## Utilisation

1. Installer l'extension [ici](https://marketplace.visualstudio.com/items?itemName=dmnchzl.reminders) ;
2. Ajouter un nouveau rappel (voir la section _Raccourcis Clavier_) ;
3. Afficher tous les rappels (en cliquant sur l'élément de la barre d'état) ;
4. Attendez... Si votre rappel contient le motif `12am`, `6pm`, `20:45` or `8h15`, il s'affichera en temps et en heure !
5. Vous pouvez également effacer tous les rappels, ou fermer et rouvrir votre IDE pour effacer la liste.

**Enjoy !**

### Paramètres de l'Extension

- `reminders.list` : Liste des rappels
- `reminders.preserve` : Paramètre facultatif qui conserve la liste des rappels
- `reminders.shift` : Paramètre facultatif pour afficher le rappel avant son échéance

### Raccourcis Clavier

- `Shift + Alt + R` : Ajouter un nouveau rappel

### Langues Supportées

- [x] Anglais
- [x] Français
- [x] Espagnol

## [Change Log](https://github.com/DmnChzl/Reminders/blob/master/CHANGELOG.md)

## Licence

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
