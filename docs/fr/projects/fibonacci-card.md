---
title: "Fibonacci Card 🃏"
releaseDate: 2021-09-23

prev:
  text: "[DIY] State Management - Solid 🌀"
  link: "/fr/projects/diy-context"
next:
  text: "Minimal Google Map"
  link: "/fr/projects/mnml-gmap"
---

# {{ $frontmatter.title }} <Badge type="tip">svelte</Badge><Badge type="tip">javascript</Badge>

> 1.1.1

J'ai réalisé une application simple et colorée (#ForTheLulz) basée sur la séquence de **Fibonacci**, pour l'évaluation (aléatoire) de toutes vos user stories dans un processus **Scrum**.

Cette application est propulsée par [SvelteJS](https://svelte.dev/).

## Aperçu

![Cards](/images/cards.webp)

## Processus

Dépôt:

```
git clone https://github.com/DmnChzl/FibonacciCard.git
```

Installation:

```
yarn install
```

Développement:

```
yarn dev
```

Compilation:

```
yarn build
```

Enjoy! 👌

### Astuce

Essayez de réduire la séquence de **Fibonacci** en utilisant `urlSearchParam` :

- [fibonacci-card.vercel.app/?limit=55](https://fibonacci-card.vercel.app/?limit=55)

Essayez le mode _script_ en utilisant `urlSearchParam` :

- [fibonacci-card.vercel.app/?mode=script](https://fibonacci-card.vercel.app/?mode=script)

## Licence

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
