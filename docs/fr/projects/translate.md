---
title: "Translate"
releaseDate: 2025-01-25

prev:
  text: "Projects"
  link: "/projects"
next:
  text: "Mocca (S)CSS - Unopinionated x Utility First CSS Library"
  link: "/projects/mocca-css"
---

# {{ $frontmatter.title }} <Badge type="tip">preact</Badge><Badge type="tip">tailwind</Badge><Badge type="tip">testing</Badge><Badge type="tip">typescript</Badge>

![Translate](/images/translate.webp)

Né d'un "_délire de développeur_", [Translate](https://translate.dmnchzl.dev) est une copie conforme (visuellement parlant) de **Google Translate**, à la différence près que cette application convertie (non pas des langues mais) :

- Des chaînes de caractères en binaire, base64, morse, etc...
- Des valeurs numériques en valeurs octales/hexadécimales, etc...
- Des dates en timestamp (et vice-versa) ;
- Ainsi que des couleurs en nuances de saturation, de luminosité, de blancheur et de noirceur.

Cette application est propulsée par [Preact](https://preactjs.com/) et [TailwindCSS](https://tailwindcss.com/).

Tests inclus avec [Playwright](https://playwright.dev/).

## Processus

Dépôt:

```bash
git clone https://github.com/DmnChzl/Translate.git
```

Installation:

```bash
pnpm install
```

Développement:

```bash
pnpm run dev
```

Tests Unitaires:

```bash
pnpm run test
```

Tests E2E:

```bash
pnpm dlx playwright install
pnpm run test:e2e
```

Compilation:

```bash
pnpm run build
```

Aperçu:

```bash
pnpm run preview
```

## License

```
Copyright (c) 2025 Damien Chazoule

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```
