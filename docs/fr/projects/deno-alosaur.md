---
title: "Deno x Alosaur 🦕🦖"
releaseDate: 2020-05-24

prev:
  text: "[DIY] State Management - Svelte 🌀"
  link: "/fr/projects/diy-stores"
next:
  text: "Awesome Hooks"
  link: "/fr/projects/awesome-hooks"
---

# {{ $frontmatter.title }} <Badge type="tip">deno</Badge><Badge type="tip">mongodb</Badge><Badge type="tip">typescript</Badge>

J'ai fait des APIs **REST** basées sur ma propre discographie (et mes goûts musicaux), avec ces technologies:

- **Deno**: Un runtime sécurisé pour JavaScript et TypeScript
  - [http://deno.land/](http://deno.land/)
- **Alosaur**: Framework Web Deno avec de nombreux décorateurs
  - [https://github.com/alosaur/](https://github.com/alosaur/alosaur/)

## Process

Cloner le projet :

```
git clone https://github.com/DmnChzl/DenoAlosaur.git
```

Exécuter le projet:

```
deno run --allow-env --allow-read --allow-write --allow-net --allow-plugin --config tsconfig.lib.json --unstable app.ts
```

### Note(s)

_Cette application dépend d'une base de données NoSQL. Installez MongoDB et exécutez les commandes suivantes dans un nouveau terminal avant de lancer le projet :_

```
mkdir DataBase
mongo --dbpath DataBase
```

_Cette application prend en charge le fichier `.env`. Veuillez le créer et remplacer les propriétés suivantes avant de lancer le projet :_

```
DENO_ENV=prod
DENO_HOST=localhost
DENO_PORT=8080
DB_NAME=deno_land
DB_HOST=localhost
DB_PORT=27017
SECRET=HelloWorld
```

## Licence

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
