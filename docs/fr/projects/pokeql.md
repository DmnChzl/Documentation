---
title: PokeQL
releaseDate: 2019-05-14

prev:
  text: Awesome Hooks"
  link: "/fr/projects/awesome-hooks"
next:
  text: "MDWrapper"
  link: "/fr/projects/mdwrapper"
---

# {{ $frontmatter.title }} <Badge type="tip">graphql</Badge><Badge type="tip">vue</Badge><Badge type="tip">typescript</Badge>

J'ai réalisé une application **Pokédex** basée sur ma propre progression dans le jeu **Pokémon** Go, avec ces technologies :

- **ExpressJS**: Framework Web rapide, dogmatique et minimaliste pour NodeJS
  - [http://expressjs.com/](http://expressjs.com/)
- **Mongoose**: Modélisation d'objets MongoDB élégante pour NodeJS
  - [https://mongoosejs.com/](https://mongoosejs.com/)
- **GraphQL**: Un langage de requête pour votre API
  - [https://graphql.org/](https://graphql.org/)
- **TypeScript**: TypeScript est un sur-ensemble typé de JavaScript qui se compile en JavaScript brut
  - [https://www.typescriptlang.org/](https://www.typescriptlang.org/)
- **VueJS**: Le Framework JavaScript Progressif
  - [https://vuejs.org/](https://vuejs.org/)
- **Apollo**: Remplacez de nombreuses APIs inflexibles par un seul système de requête polyvalent
  - [https://www.apollographql.com/](https://www.apollographql.com/)

## Processus

Cloner le projet :

```
git clone https://github.com/dmnchzl/pokeql.git
```

### Parties

| **Front-End**                                                                 | **Back-End**                                                                 |
| ----------------------------------------------------------------------------- | ---------------------------------------------------------------------------- |
| [Go To ReadMe](https://github.com/DmnChzl/PokeQL/blob/master/Front/README.md) | [Go To ReadMe](https://github.com/DmnChzl/PokeQL/blob/master/Back/README.md) |

## Licence

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
