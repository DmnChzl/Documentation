# MongoDB

## Lancer MongoDB avec un chemin de base de données

```shell
mongod --dbpath "C:/Program Files/MongoDB/Data/DataBase"
```

## Lancer le client Mongo

```shell
mongo
```

## Voir les actions disponibles sur la base de données

```shell
db.helps()
```

## Voir les bases de données disponibles

```shell
show dbs
```

## Utiliser une base de données

```shell
use dmnchzl
```

## Voir la base de données utilisée

```shell
db
```

## Voir les collections disponibles dans la base de données

```shell
show collections
```

## Créer des documents

```shell
db.apps.insert({_id:1, label:"Torch", color:"Red", available:false})
db.apps.insert({_id:2, label:"Decode", color:"Red", available:false})
db.apps.insert({_id:3, label:"PadLock", color:"Orange", available:false})
```

## Voir le contenu de la collection

```shell
db.apps.find()
{_id:1, label:"Torch", color:"Red", available:false}
{_id:2, label:"Decode", color:"Red", available:false}
{_id:3, label:"PadLock", color:"Orange", available:false}
```

## Ajouter une propriété dans un document

```shell
db.apps.update({label:"Torch"}, {$set: {tab:["A","B","C"]}})
```

## Modifier une propriété dans plusieurs documents

```shell
db.apps.update({available:false}, {$set: {available:true}}, {multi:true})
```

## Voir le contenu de la table de manière décroissante

```shell
db.apps.find({label: {$in: ["Torch","Decode","PadLock"]}}).sort({_id:-1})
{_id:3, label:"PadLock", color:"Orange", available:true}
{_id:2, label:"Decode", color:"Red", available:true}
{_id:1, label:"Torch", color:"Red", available:true, tab:["A","B","C"]}
```

## Ajouter une valeur dans un tableau

```shell
db.apps.update({label:"Torch"}, {$push: {tab:"D"}})
```

## Ajouter une même valeur dans un tableau sans doublon

```shell
db.apps.update({label:"Torch"}, {$addToSet: {tab:"D"}})
```

## Visualiser l'élément possédant le tableau proprement

```shell
db.apps.find({label:"Torch"}).pretty()
{
     _id:1,
     label:"Torch",
     color:"Red",
     available:true,
     tab:[
         "A",
         "B",
         "C",
         "D"
     ]
}
```

## Supprimer la dernière valeur d'un tableau

```shell
db.apps.update({label:"Torch"}, {$pop: {tab:1}})
```

## Supprimer une propriété dans un document

```shell
db.apps.update({label:"Torch"}, {$unset: {tab:1}})
```

## Supprimer un document d'une collection

```shell
db.apps.remove({label:"Torch"})
```

## Supprimer une collection

```shell
db.apps.drop()
```

## Supprimer une base de données

```shell
db.runCommand({dropDatabase:1})
```
