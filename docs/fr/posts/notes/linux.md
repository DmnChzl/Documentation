# Linux

## Afficher "Hello World"

```shell
echo "Hello" && echo 'World !'
```

## Ecrire un fichier

```shell
# nano file.txt
vim file.txt
```

## Ecraser le contenu d'un fichier et ajouter une nouvelle ligne

```shell
echo "Erase content" > file.txt && echo "Add this" >> file.txt
```

## Lire le contenu d'un fichier

```shell
cat -n file.txt
```

## Rechercher dans un fichier

```shell
grep -n "this" file.txt
```

## Demander les droits "ROOT"

```shell
sudo su
```

## Désarchiver une archive

```shell
tar -zxvf tar.gz
```

## Forcer la suppression d'un fichier

```shell
rm -rf document.txt
```

## Copier un fichier

```shell
cp file.txt /home/user/download/file.txt
```

## Déplacer un fichier

```shell
mv file.txt /home/user/download/document.txt
```

## Configurer le clavier en "AZERTY"

```shell
setxkbmap fr
```

## Voir l'historique des commandes

```shell
cat .bash_history
```

## Voir les logs d'erreurs "NGINX"

```shell
cat /var/log/nginx/error.log
```

## Voir les processus "NGINX" en cours

```shell
ps aux | grep nginx
```

## Créer un fichier d'authentification basique

```shell
sudo sh -c "echo -n 'username:' >> /etc/nginx/.htpasswd"
```

## Ajouter le mot de passe au fichier d'authentification basique

```shell
sudo sh -c "openssl passwd -apr1 >> /etc/nginx/.htpasswd"
```

## Obtenir la date courante

```shell
date
```

## Afficher la date formatée

```shell
echo "$(date + '%d/%m/%Y %H:%M')"
```

## Éditer les tâches planifiées

```shell
crontab -e
```

## Exécuter un script tout les jours à minuit

```shell
# m h dom mon dow command
0 0 * * * /home/script.sh
```

## Fichier "DOS" Vers "UNIX"

```
sed -i 's/\r//'
```

## Transférer un ficher via SSH

```shell
scp -r -p /path/to/file user@host:/path/to/dest
```

## Compter le nombre de lignes dans un projet "GIT"

```shell
git ls-files | xargs cat | wc -l
```

## Compter le nombre de lignes 'JavaScript' dans un projet "GIT" / par fichier

```shell
git ls-files *.js | xargs wc -l
```
