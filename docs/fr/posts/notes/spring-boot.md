# Spring Boot

Extension de Spring 🌱 qui simplifie la création d'applications Java ☕ autonomes et prêtes à l'emploi. Il élimine la configuration complexe grâce à :

- **Convention Over Configuration** ;
- Un serveur embarqué (**Tomcat**, **Jetty**) ;
- Un système d'"**Auto-Configuration**" intelligent ;

Il suffit de l'annotation `@SpringBootApplication` pour démarrer un projet rapidement...

## Spring 🌱

Framework Open-Source pour le développement d'applications Java ☕, axé sur la création de systèmes flexibles, testables et facilement maintenable. Il repose sur des concepts clés comme l'[Inversion of Control](#inversion-of-control) (IoC) et l'**Injection de Dépendances** (DI) pour simplifier la gestion des composants. Spring 🌱 offre également des modules pour la gestion des données ([Spring Data](#spring-data)), la sécurité ([Spring Security](#spring-security)), et bien plus encore...

## Bean

Un **Bean** est un objet géré par le **Spring IoC Container**. Il est instancié, configuré et géré par le conteneur tout au long de son cycle de vie à la manière d'un **Singleton** (c'est-à-dire qu'une seule instance est créée pour tout le conteneur). Les **Bean**'s sont créés à partir de classes annotées avec des stéréotypes (`@interface`) comme `@Component`, `@Service`, `@Repository` ou définis via `@Bean` dans une classe de configuration.

Cycle de Vie :

1. **Instantiation** : Le conteneur crée une instance du **Bean** ;
2. **Injection des Dépendances** : Les dépendances sont injectées (via `@Autowired` ou constructeur) ;
3. **Initialisation** : La méthode annotée avec `@PostConstruct` est exécutée ;
4. **Utilisation** : Le **Bean** est prêt à être utilisé dans l'application ;
5. **Destruction** : Avant sa suppression, la méthode annotée avec `@PreDestroy` est appelée ;

## Inversion of Control

L'**Inversion of Control** (IoC) est un principe de conception où le contrôle de l'instanciation et de la gestion des dépendances est délégué à un conteneur externe plutôt qu'au code lui-même.

```java
class Engine {
    void start() {
        System.out.println("Starting...");
    }
}

class Robot {
    private Engine engine;

    public Robot() {
        this.engine = new Engine();
    }

    void run() {
        engine.start();
        System.out.println("Running...");
    }
}

public class Main {
    public static void main(String[] args) {
        Robot robot = new Robot();
        robot.run();
    }
}
```

> ❌ Sans Injection de Dépendance : **Couplage Fort** !

```java
class Engine {
    void start() {
        System.out.println("Starting...");
    }
}

class Robot {
    private final Engine engine;

    public Robot(Engine engine) {
        this.engine = engine;
    }

    void run() {
        engine.start();
        System.out.println("Running...");
    }
}

public class Main {
    public static void main(String[] args) {
        Engine engine = new Engine();
        Robot robot = new Robot(engine);
        robot.run();
    }
}
```

> ✅ Avec Injection de Dépendance : **Couplage Faible** !

## Annotations ⚙️

### Spring Core

| **Annotation**           | **Description**                                                                                                      |
| ------------------------ | -------------------------------------------------------------------------------------------------------------------- |
| `@Configuration`         | Indique qu'une classe contient des définitions de **Bean**'s via des méthodes annotées `@Bean`                       |
| `@Bean`                  | Déclare un **Bean** manuel dans le contexte Spring 🌱 (souvent utilisé dans les classes `@Configuration`)            |
| `@Component`             | Marque une classe comme un **Bean** détectable automatiquement lors de la détection de composants (`@ComponentScan`) |
| `@Autowired`             | Effectue l'**Injection de Dépendances** automatique par le conteneur Spring 🌱                                       |
| `@Qualifier("beanName")` | Spécifie quel **Bean** injecter lorsque plusieurs implémentations sont disponibles                                   |
| `@PostConstruct`         | Méthode exécutée après l'instanciation du **Bean** (utile pour l'initialisation de données)                          |
| `@PreDestroy`            | Méthode exécutée juste avant la destruction du **Bean** (utile pour fermer des connexions)                           |

> Hors "**Spring Core**" Strict...

| **Annotation**                                | **Description**                                                                     |
| --------------------------------------------- | ----------------------------------------------------------------------------------- |
| `@PropertySource("classpath:app.properties")` | Charge un fichier de propriétés externe dans le contexte Spring 🌱                  |
| `@Value("${property.name}")`                  | Injecte une valeur depuis un fichier de propriétés dans un champ ou un paramètre    |
| `@Service`                                    | Spécialisation de `@Component`, utilisée pour marquer les classes de logique métier |

### Spring Data

> Gestion des Entités

| **Annotation**   | **Description**                                                                                                                                   |
| ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| `@Repository`    | Indique que la classe est un "Repository" **JPA**, un composant qui encapsule l'accès aux données (**DAO**)                                       |
| `@Entity`        | Marque une classe Java ☕ comme une entité **JPA** liée à une table en base de données                                                            |
| `@Table`         | Définit le nom de la table SQL associée à l'entité                                                                                                |
| `@Id`            | Définit l'attribut comme la clé primaire de l'entité                                                                                              |
| `@Column`        | Spécifie des propriétés supplémentaires pour la colonne associée à un attribut (nom, longueur, etc...)                                            |
| `@Transactional` | Marque une méthode ou une classe comme étant "Transactionnelle", ce qui permet de gérer l'atomicité des données pendant l'exécution de la méthode |

> Gestion des Relations

| **Annotation** | **Description**                                                                        |
| -------------- | -------------------------------------------------------------------------------------- |
| `@OneToOne`    | Définit une relation "un à un" entre deux entités                                      |
| `@OneToMany`   | Définit une relation "un à plusieurs" entre une entité et plusieurs autres entités     |
| `@ManyToOne`   | Définit une relation "plusieurs à un" entre plusieurs entités et une entité principale |
| `@ManyToMany`  | Définit une relation plusieurs à plusieurs entre deux entités                          |

### Spring MVC

> Gestion du Contrôleur et de la Réponse

| **Annotation**    | **Description**                                                                                             |
| ----------------- | ----------------------------------------------------------------------------------------------------------- |
| `@Controller`     | Définit une classe comme contrôleur pour gérer les requêtes Web                                             |
| `@RestController` | Combine `@Controller` et `@ResponseBody` pour créer des API REST où les réponses sont en `application/json` |
| `@ResponseBody`   | Indique que la valeur de retour d'une méthode est directement écrite dans la réponse HTTP                   |
| `@ResponseHeader` | Permet d'ajouter des en-têtes HTTP personnalisés dans la réponse                                            |

> Gestion du Mapping de la Requête

| **Annotation**    | **Description**                                                                    |
| ----------------- | ---------------------------------------------------------------------------------- |
| `@RequestMapping` | Mapping général d'une requête HTTP (GET, POST, etc...) à une méthode ou classe     |
| `@GetMapping`     | Raccourci de `@RequestMapping(method = RequestMethod.GET)` pour les requêtes GET   |
| `@PostMapping`    | Raccourci de `@RequestMapping(method = RequestMethod.POST)` pour les requêtes POST |

> Gestion des Paramètres et du Cors de la Requête

| **Annotation**   | **Description**                                                                   |
| ---------------- | --------------------------------------------------------------------------------- |
| `@PathVariable`  | Extrait une valeur dynamique de l'URL                                             |
| `@RequestParam`  | Récupère les paramètres de requête                                                |
| `@RequestBody`   | Convertit le corps de la requête `application/json` en un objet Java ☕           |
| `@RequestHeader` | Extrait les en-têtes HTTP de la requête (`Authorization`, `Content-Type`, etc...) |

### Spring Security

> Gestion de la Sécurité

| **Annotation**                | **Description**                                                                     |
| ----------------------------- | ----------------------------------------------------------------------------------- |
| `@EnableWebSecurity`          | Active la configuration de **Spring Security** pour sécuriser les requêtes Web      |
| `@EnableGlobalMethodSecurity` | Active la sécurité au niveau des méthodes (via `@Secured`, `@PreAuthorize`, etc...) |

> Gestion du Contrôle d'Accès aux Méthodes

| **Annotation**               | **Description**                                                                             |
| ---------------------------- | ------------------------------------------------------------------------------------------- |
| `@Secured("ROLE_ADMIN")`     | Restreint l'accès à la méthode aux utilisateurs ayant le rôle spécifié                      |
| `@RolesAllowed("ROLE_USER")` | Equivalent à `@Secured`, mais conforme à la spécification **JSR-250**                       |
| `@PreAuthorize`              | Évalue une expression **Spring Expression Language** (SpEL) avant l'exécution de la méthode |
| `@PostAuthorize`             | Évalue une expression **Spring Expression Language** (SpEL) après l'exécution de la méthode |

### Exception Handler

| **Annotation**      | **Description**                                                                                            |
| ------------------- | ---------------------------------------------------------------------------------------------------------- |
| `@ControllerAdvice` | Définit une classe globale pour gérer les exceptions, les "bindings" et les logiques applicatives communes |
| `@ExceptionHandler` | Indique qu'une méthode est un gestionnaire d'exceptions pour un ou plusieurs types d'exceptions spécifiés  |

### Cache

> Gestion du "Cache Manager" (**EhCache** / **Caffeine** / **Redis**)

| **Annotation**   | **Description**                                                                                                                                                             |
| ---------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `@EnableCaching` | Active la gestion du cache dans une application Spring 🌱, ce qui permet d'utiliser les annotations de cache sur les méthodes                                               |
| `@Cacheable`     | Indique que le résultat d'une méthode doit être mis en cache ; Si la méthode est appelée avec les mêmes arguments, le cache est retourné au lieu d'exécuter la méthode      |
| `@CacheEvict`    | Permet de supprimer un ou plusieurs éléments du cache ; Cette annotation peut être utilisée après l'exécution d'une méthode pour invalider le cache d'une ou plusieurs clés |
| `@CachePut`      | Met à jour le cache après l'exécution d'une méthode ; Contrairement à `@Cacheable`, la méthode s'exécute toujours et le résultat est ensuite mis à jour dans le cache       |

### Lombok 🌶️

| **Annotation**        | **Description**                                                                                                                |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------ |
| `@Data`               | Génère toutes les méthodes nécessaires : `@Getter`, `@Setter`, `@RequiredArgsConstructor`, `@EqualsAndHashCode` et `@ToString` |
| `@Getter`             | Génère un "Getter" pour tous les attributs de la classe                                                                        |
| `@Setter`             | Génère un "Setter" pour tous les attributs de la classe                                                                        |
| `@Builder`            | Génère un **Builder** pour créer des objets de manière fluide (même avec des arguments nommés)                                 |
| `@NoArgsConstructor`  | Génère un constructeur sans argument pour la classe                                                                            |
| `@AllArgsConstructor` | Génère un constructeur avec tous les arguments (tous les champs de la classe)                                                  |
| `@EqualsAndHashCode`  | Génère les méthodes `equals()` et `hashCode()` basées sur les attributs de la classe                                           |
| `@ToString`           | Génère la méthode `toString()` qui retourne une représentation textuelle de l'objet                                            |

### Simple Logging Facade For Java

| **Annotation** | **Description**                                                                                          |
| -------------- | -------------------------------------------------------------------------------------------------------- |
| `@Slf4j`       | Génère un objet `Logger` avec le nom de la classe pour faciliter l'enregistrement des logs via **SLF4J** |

### JUnit

> Gestion des Tests Unitaires avec **Mockito**

| **Annotation**               | **Description**                                                                                                                                              |
| ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `@RunWith`                   | Spécifie un "Runner" pour exécuter les tests (`@RunWith(MockitoJUnitRunner.class)`, `@RunWith(SpringRunner.class)`)                                          |
| `@ExtendWith`                | Utilisé avec **JUnit** v5 pour intégrer des extensions, comme `@ExtendWith(SpringExtension.class)` pour intégrer Spring dans les tests unitaires             |
| `@SpringBootTest`            | Indique que le test doit charger le contexte **Spring Boot** pour tester l'application en environnement intégré                                              |
| `@MockBean` / `@MockitoBean` | Crée un Mock pour une dépendance Spring 🌱 et l'injecte dans le contexte d'application                                                                       |
| `@Mock`                      | Utilisé pour créer des Mocks d'objets dans les tests avec **Mockito**                                                                                        |
| `@InjectMocks`               | Indique à **Mockito** d'injecter les Mocks dans l'objet testé (généralement un service ou un contrôleur)                                                     |
| `@AutoConfigureMockMvc`      | Active la configuration de **MockMVC** pour les tests d'intégration des contrôleurs **Spring MVC** sans démarrer un serveur                                  |
| `@WebMvcTest`                | Lance un test limité à la couche **Web MVC** de Spring, en se concentrant sur les contrôleurs et leurs interactions HTTP (sans le contexte de l'application) |
| `@AutoConfigureWireMock`     | Active la configuration de **WireMock** pour simuler des services externes (API, bases de données, etc...) dans les tests d'intégration                      |

## Spring WebFlux 🍃

**Spring WebFlux** est un framework réactif de Spring 🌱 qui permet de développer des applications Web asynchrones et non-bloquantes. Il repose sur le **Project Reactor**, qui implémente les **Reactive Streams**, une spécification pour le traitement asynchrone de flux de données.

- **Project Reactor** est une bibliothèque Java ☕ réactive qui fournit les abstractions de **Mono** (pour un seul élément) et **Flux** (pour un flux d'éléments) pour la gestion de données de manière réactive ;
- `Mono<T>` est utilisé pour représenter un flux contenant zéro ou un élément ;
- `Flux<T>` est utilisé pour des flux contenant zéro à plusieurs éléments ;
- **WebClient** est un composant de **Spring WebFlux** qui permet de faire des appels HTTP de manière asynchrone et non-bloquante ;
