---
layout: doc

prev:
  text: "UUID"
  link: "/posts/snippets/javascript/uuid"
next:
  text: "Binary Parser"
  link: "/posts/snippets/typescript/binary-parser"
---

# TypeScript

TypeScript 💙 est un sur-ensemble à "typage statique" de JavaScript, compilé en JS pour s'exécuter dans n'importe quel environnement compatible. Créé en 2012 par **Microsoft** (et popularisé par **Angular** en 2014), il vise à améliorer la qualité et la maintenabilité du code, tout en conservant la flexibilité de JavaScript.
