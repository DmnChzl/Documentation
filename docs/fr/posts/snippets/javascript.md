---
layout: doc

prev:
  text: "Singleton"
  link: "/posts/snippets/java/singleton"
next:
  text: "Curry"
  link: "/posts/snippets/javascript/curry"
---

# JavaScript

JavaScript 💛 est un langage de programmation interprété à "typage dynamique", principalement utilisé pour le développement Web. Créé en 1995 par **Brendan Eich** chez **NetScape**, il est aujourd'hui l'un des langages les plus populaires, fonctionnant aussi bien côté client que côté serveur grâce à des environnements comme **NodeJS**.
