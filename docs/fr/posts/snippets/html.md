---
layout: doc

prev:
  text: "Commit Message"
  link: "/posts/snippets/bash/commit-msg"
next:
  text: "File Upload"
  link: "/posts/snippets/html/file-upload"
---

# HTML

**HTML** signifie « _HyperText Markup Language_ » qu'on peut traduire par « langage de balises pour l'hypertexte ». Il est utilisé afin de créer et de représenter le contenu d'une page web et sa structure. D'autres technologies sont utilisées avec HTML pour décrire la présentation d'une page (CSS) et/ou ses fonctionnalités interactives (JavaScript).
