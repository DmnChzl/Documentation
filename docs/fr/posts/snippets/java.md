---
layout: doc

prev:
  text: "Virtual DOM"
  link: "/posts/snippets/html/virtual-dom"
next:
  text: "Builder Pattern"
  link: "/posts/snippets/java/builder-pattern"
---

# Java

Java ☕ est un langage de programmation orienté objet à "typage statique" et une plateforme d’exécution conçue pour être portable, sécurisée et performante. Il a été développé par **Sun MicroSystems** en 1995 et est maintenu par **Oracle** (`javax.*`) jusqu'en 2017, puis par l'**Eclipse Foundation** (`jakarta.*`) jusqu'à aujourd'hui.
