# Cases

> Lorem ipsum dolor sit amet

## toCamelCase

> loremIpsumDolorSitAmet

```ts
const toCamelCase = (str: string): string => {
  const regExp = /[^a-zA-Z0-9]+(.)/g; // Separator with next char capture pattern
  return str.toLowerCase().replace(regExp, (_match, char) => char.toUpperCase());
};
```

## toKebabCase

> lorem-ipsum-dolor-sit-amet

```ts
const toKebabCase = (str: string): string => {
  const regExp = /[^a-zA-Z0-9]+/g; // Non alphanumeric separator pattern
  return str.toLowerCase().replace(regExp, "-").replace(/^-|-$/g, "");
};
```

## toPascalCase

> Lorem Ipsum Dolor Sit Amet

```ts
const toPascalCase = (str: string): string => {
  const regExp = /(^\w|[^a-zA-Z0-9]+(\w))/g; // First or separator followed by char pattern
  return str.toLowerCase().replace(regExp, (_match, char) => char.toUpperCase());
};
```

## toSnakeCase

> lorem_ipsum_dolor_sit_amet

```ts
const toSnakeCase = (str: string): string => {
  const regExp = /[^a-zA-Z0-9]+/g; // Non alphanumeric separator pattern
  return str.toLowerCase().replace(regExp, "_").replace(/^_|_$/g, "");
};
```
