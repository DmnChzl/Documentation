# Sort By Key

## sortBy.ts

```ts
// prettier-ignore
export const sortBy = <T>(key: keyof T) => (a: T, b: T) => a[key] > b[key] ? 1 : a[key] < b[key] ? -1 : 0;
```
