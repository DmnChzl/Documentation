# DenoLand Server

[Deno](https://deno.land) REST API (Based On Pizza Management Of Course...) 🍕

## server.ts

```ts
import { Application, HandlerFunc, Context } from "https://deno.land/x/abc/mod.ts";

interface Pizza {
  label: string;
  items: string[];
  price: number;
}

let pizzas: Pizza[] = [
  {
    label: "4_Cheeses",
    items: ["Mozzarella", "Goat_Cheese", "Reblochon", "Gorgonzola"],
    price: 14.9
  },
  {
    label: "Atlas",
    items: ["Mozzarella", "Spicy_Chicken", "Chorizo", "Marinated_Zucchini", "Ras-El-Hanout Spices"],
    price: 13.6
  },
  {
    label: "Barcelona",
    items: ["Mozzarella", "Chorizo", "Fried_Onions", "Peppers", "Olives"],
    price: 13.3
  },
  {
    label: "Basque",
    items: ["Mozzarella", "Spicy_Chicken", "Peppers", "Candied_Tomatoes"],
    price: 13.6
  },
  {
    label: "Bolognese",
    items: ["Mozzarella", "Tomato_Pulp", "Ground_Beef", "Peppers", "Bacon", "Mushrooms", "Fried_Onions", "Olives"],
    price: 14.3
  },
  {
    label: "Brunch",
    items: ["Mozzarella", "Grilled_Bacon", "Smoked_Sausage", "Candied_Tomatoes", "Egg", "Fried_Onions"],
    price: 13.9
  },
  {
    label: "Calzone",
    items: ["Mozzarella", "White_Ham", "Mushrooms", "Cheese", "Egg", "Oregano"],
    price: 13.4
  },
  {
    label: "Cheese_Burger",
    items: [
      "Tomato_Sauce",
      "Sweet_Mustard",
      "Mozzarella",
      "Cheddar",
      "Ground_Beef",
      "Sweet_And_Sour_Pickles",
      "Red_Onions",
      "Burger_Sauce"
    ],
    price: 14.9
  },
  {
    label: "Flamenkuche",
    items: ["Mozzarella", "Bacon", "Cheese", "Fried_Onions"],
    price: 13.3
  },
  {
    label: "Gorgone",
    items: ["Mozzarella", "Gorgonzola", "Italian Raw_Ham", "Nut"],
    price: 12.6
  },
  {
    label: "Indian",
    items: ["Mozzarella", "Spicy Chicken", "Peppers", "Cashew_Nut", "Coriander"],
    price: 12.8
  },
  {
    label: "Kebab",
    items: ["Mozzarella", "Kebab_Chicken", "Red_Onions", "Kebab_Sauce"],
    price: 12.5
  },
  {
    label: "Lola",
    items: ["Fresh_Cream", "Mozzarella", "Bacon", "Cheese"],
    price: 6.9
  },
  {
    label: "Margherita",
    items: ["Mozzarella", "Basilic"],
    price: 9.2
  },
  {
    label: "Nordic",
    items: ["Salmon Cream", "Mozzarella", "Cooked_Sorrel", "Fresh_Salmon_Dice", "Fried_Onions"],
    price: 14.9
  },
  {
    label: "Overseas",
    items: ["Mozzarella", "Chicken", "Ananas", "Curry"],
    price: 13.1
  },
  {
    label: "Périgourdine",
    items: ["Mozzarella", "Potatoes", "Smoked_Duck_Breast", "Candied_Gizzards", "Fried_Onions"],
    price: 14.1
  },
  {
    label: "Pollen",
    items: ["Mozzarella", "Goat_Cheese", "Honey", "Nut"],
    price: 11.8
  },
  {
    label: "Raclette",
    items: ["Mozzarella", "Potatoes", "Italian_Ham", "Raclette_Cheese", "Fried_Onions"],
    price: 14.6
  },
  {
    label: "Regina",
    items: ["Mozzarella", "Mushrooms", "White_Ham", "Olives"],
    price: 13.9
  },
  {
    label: "Sasha",
    items: ["Tomato_Sauce", "Mozzarella", "White_Ham", "Cheese"],
    price: 6.9
  },
  {
    label: "Sardegna",
    items: ["Mushrooms", "Smoked_Cheese", "Italian Raw_Ham", "Olives"],
    price: 13.9
  },
  {
    label: "Tartiflette",
    items: ["Mozzarella", "Potatoes", "Bacon", "Reblochon", "Fried_Onions"],
    price: 14.1
  },
  {
    label: "Truffade",
    items: ["Mozzarella", "Potatoes", "Smoked_Sausage", "Tome_Of_Auvergne", "Fried_Onions"],
    price: 14.1
  },
  {
    label: "Veggie",
    items: ["Mozzarella", "Peppers", "Mushrooms", "Zucchini", "Cherry_Tomatoes", "Artichokes"],
    price: 13.9
  }
];

type PizzaOrUndefined = Pizza | undefined;

const getAllPizzas: HandlerFunc = (context: Context) => context.json(pizzas, 200);

const getPizza: HandlerFunc = (context: Context) => {
  const { label } = context.params as { label: string };
  const pizza: PizzaOrUndefined = pizzas.find((p: Pizza) => p.label === label);

  if (pizza) {
    return context.json(pizza, 200);
  }

  return context.json({ message: "Pizza Not Found" }, 404);
};

const addPizza: HandlerFunc = async (context: Context) => {
  const body = await context.body();

  if (Object.keys(body).length === 0) {
    return context.json({ message: "Empty" }, 400);
  }

  const { label, items, price } = body;

  pizzas = [...pizzas, { label, items, price } as Pizza];

  return context.json({ label, items, price }, 201);
};

const upPizza: HandlerFunc = async (context: Context) => {
  const { label } = context.params as { label: string };
  let pizza: PizzaOrUndefined = pizzas.find((p: Pizza) => p.label === label);

  if (pizza) {
    const body = await context.body();

    if (Object.keys(body).length === 0) {
      return context.json({ message: "Empty" }, 400);
    }

    pizzas = pizzas.map((p: Pizza) => (p.label === label ? { ...p, ...body } : p));
    return context.json({ message: "Okay" }, 200);
  }

  return context.json({ message: "Pizza Not Found" }, 404);
};

const delPizza: HandlerFunc = (context: Context) => {
  const { label } = context.params as { label: string };
  pizzas = pizzas.filter((p: Pizza) => p.label !== label);
  return context.json({ message: "Okay" }, 200);
};

const app = new Application();

app
  .get("/pizzas", getAllPizzas)
  .get("/pizzas/:label", getPizza)
  .post("/pizzas", addPizza)
  .put("/pizzas/:label", upPizza)
  .delete("/pizzas/:label", delPizza)
  .start({ port: 5050 });

console.log("Server Is Running On Port :5050");
```
