---
layout: doc

prev:
  text: "UUID"
  link: "/posts/snippets/javascript/uuid"
next:
  text: "Binary Parser"
  link: "/posts/snippets/typescript/binary-parser"
---

# TypeScript

TypeScript 💙 is a "statically typed" superset of JavaScript, compiled into JS to run in any compatible environment. Created in 2012 by **Microsoft** (and popularized by **Angular** in 2014), it aims to improve code quality and maintainability while retaining the flexibility of JavaScript.
