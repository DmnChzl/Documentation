# BlockChain

Using [Deno](https://deno.land) v1

## blockChain.ts

```ts
import { createHash } from "https://deno.land/std/hash/mod.ts";

/**
 * Generate SHA-256
 * @param {string} message
 * @returns {string}
 */
const generateHash = (message: string): string => {
  return createHash("sha256").update(message).toString();
};

class Block {
  timeStamp: string;
  data: string[];
  prevHash: string;
  hash: string;

  constructor(timeStamp = "", data: string[] = []) {
    this.timeStamp = timeStamp;
    this.data = data;
    this.prevHash = "";
    this.hash = this.getHash();
  }

  getHash(): string {
    return generateHash(this.timeStamp + JSON.stringify(this.data) + this.prevHash);
  }

  toString(): string {
    return `[BLOCK]
    TimeStamp:     ${this.timeStamp}
    Data:          ${JSON.stringify(this.data)}
    Previous Hash: ${this.prevHash}
    Hash:          ${this.hash}
    `;
  }
}

class BlockChain {
  chain: Block[];

  constructor() {
    this.chain = [this.getGenesisBlock()];
  }

  getGenesisBlock(): Block {
    const now = new Date().getTime().toString();
    return new Block(now);
  }

  getLastBlock(): Block {
    return this.chain[this.chain.length - 1];
  }

  addNewBlock(newBlock: Block) {
    newBlock.prevHash = this.getLastBlock().hash;
    newBlock.hash = newBlock.getHash();

    // I M M U T A B I L I T Y
    const frozenBlock = Object.freeze(newBlock);
    this.chain.push(frozenBlock);
  }

  isValid(): boolean {
    for (let i = 1; i < this.chain.length; i++) {
      const currentBlock = this.chain[i];
      const prevBlock = this.chain[i - 1];

      if (currentBlock.hash !== currentBlock.getHash() || prevBlock.hash !== currentBlock.prevHash) {
        return false;
      }
    }

    return true;
  }
}
```

## blockChain.test.ts

> `deno test blockChain.test.ts`

```ts
import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

Deno.test("BlockChain", () => {
  const myChain = new BlockChain();

  const now = new Date();

  const stBlock = new Block(now.getTime().toString(), ["Hello World"]);

  myChain.addNewBlock(stBlock);

  assertEquals(myChain.getLastBlock().hash, stBlock.hash);

  const ndBlock = new Block(now.setMinutes(now.getMinutes() + 30).toString(), ["Lorem Ipsum"]);

  myChain.addNewBlock(ndBlock);

  assertEquals(myChain.getLastBlock().hash, ndBlock.hash);

  const rdBlock = new Block(now.setHours(now.getHours() + 12).toString(), ["Dolor Sit Amet"]);

  myChain.addNewBlock(rdBlock);

  assertEquals(myChain.getLastBlock().hash, rdBlock.hash);

  assertEquals(myChain.isValid(), true);
});
```
