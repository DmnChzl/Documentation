# File Upload

## fileUpload.html

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>File Upload</title>
  </head>

  <body>
    <style>
      body {
        font-family: Helvetica, sans-serif;
        margin: 0;
      }

      input[type="file"] {
        position: absolute;
        width: 0;
        height: 0;
        overflow: hidden;
        opacity: 0;
        z-index: -1;
      }

      .upload {
        display: flex;
        align-items: center;
        justify-content: center;
        margin: 1rem;
        width: 100px;
        height: 100px;
        color: #666;
        border: 2px #666 dashed;
        border-radius: 8px;
        cursor: pointer;
      }

      .upload:hover {
        color: #999;
        border-color: #999;
      }
    </style>

    <div style="position: relative">
      <input id="upload" type="file" accept="application/json" />
      <label for="upload" class="upload">Upload</label>
    </div>

    <script type="text/javascript">
      document.getElementById("upload").addEventListener("change", event => {
        const [file] = event.target.files;
        console.log(file);
      });
    </script>
  </body>
</html>
```
