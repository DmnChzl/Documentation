---
layout: doc

prev:
  text: "Commit Message"
  link: "/posts/snippets/bash/commit-msg"
next:
  text: "File Upload"
  link: "/posts/snippets/html/file-upload"
---

# HTML

**HTML** (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content. Other technologies besides HTML are generally used to describe a web page's appearance/presentation (CSS) or functionality/behavior (JavaScript).
