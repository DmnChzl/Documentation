# Spinner

## spinner.html

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="theme-color" content="#000000" />
    <title>Spinner</title>
  </head>

  <body>
    <style>
      .spinner {
        animation: spin 1.45s linear infinite;
      }

      @keyframes spin {
        0% {
          transform: rotate(0deg);
        }

        100% {
          transform: rotate(270deg);
        }
      }

      .circle {
        stroke-dasharray: 120;
        stroke-dashoffset: 0;
        transform-origin: center;
        animation: dash 1.45s ease-in-out infinite;
      }

      @keyframes dash {
        0% {
          stroke-dashoffset: 120;
        }

        50% {
          stroke-dashoffset: 30;
          transform: rotate(145deg);
        }

        100% {
          stroke-dashoffset: 120;
          transform: rotate(450deg);
        }
      }
    </style>

    <svg
      xmlns="http://www.w3.org/2000/svg"
      class="spinner"
      width="48"
      height="48"
      stroke="currentColor"
      viewBox="0 0 48 48">
      <circle fill="none" class="circle" stroke-width="4" cx="24" cy="24" r="20"></circle>
    </svg>

    <script>
      // Red, Blue, Green, Yellow
      const COLORS = ["#ef4444", "#3b82f6", "#22c55e", "#eab308"];
      let idx = 0;

      setInterval(() => {
        const [spinner] = document.getElementsByClassName("spinner");
        spinner.style.color = COLORS[idx % 4];
        idx++;
      }, 1450);
    </script>
  </body>
</html>
```
