# Toggle Switch

## toggleSwitch.html

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="theme-color" content="#000000" />
    <title>Toggle Switch</title>
  </head>

  <body>
    <style>
      :root {
        --color-cool-gray: #6b7280;
        --color-red: #ef4444;
        --color-amber: #f59e0b;
        --color-emerald: #10b981;
        --color-blue: #3b82f6;
        --color-indigo: #6366f1;
        --color-violet: #8b5cf6;
        --color-pink: #ec4899;
      }

      .switch {
        position: relative;
        display: inline-block;
        width: 40px;
        height: 20px;
        background-color: rgba(0, 0, 0, 0.25);
        border-radius: 20px;
        transition: all 0.25s;
      }

      .switch::after {
        content: "";
        position: absolute;
        width: 16px;
        height: 16px;
        border-radius: 16px;
        background-color: #fff;
        top: 2px;
        left: 2px;
        transition: all 0.25s;
      }

      input[type="checkbox"]:checked + .switch::after {
        transform: translateX(20px);
      }

      input[type="checkbox"]:checked + .switch {
        background-color: var(--color-cool-gray);
      }

      input[type="checkbox"]:checked + .switch.red {
        background-color: var(--color-red);
      }

      input[type="checkbox"]:checked + .switch.yellow {
        background-color: var(--color-amber);
      }

      input[type="checkbox"]:checked + .switch.green {
        background-color: var(--color-emerald);
      }

      input[type="checkbox"]:checked + .switch.blue {
        background-color: var(--color-blue);
      }

      input[type="checkbox"]:checked + .switch.indigo {
        background-color: var(--color-indigo);
      }

      input[type="checkbox"]:checked + .switch.purple {
        background-color: var(--color-violet);
      }

      input[type="checkbox"]:checked + .switch.pink {
        background-color: var(--color-pink);
      }

      .offscreen {
        position: absolute;
        left: -9999px;
      }
    </style>

    <div>
      <input type="checkbox" id="toggle" class="offscreen" />
      <label for="toggle" class="switch"></label>
    </div>

    <div>
      <input type="checkbox" id="red" class="offscreen" />
      <label for="red" class="switch red"></label>
    </div>

    <div>
      <input type="checkbox" id="amber" class="offscreen" />
      <label for="amber" class="switch yellow"></label>
    </div>

    <div>
      <input type="checkbox" id="emerald" class="offscreen" />
      <label for="emerald" class="switch green"></label>
    </div>

    <div>
      <input type="checkbox" id="blue" class="offscreen" />
      <label for="blue" class="switch blue"></label>
    </div>

    <div>
      <input type="checkbox" id="indigo" class="offscreen" />
      <label for="indigo" class="switch indigo"></label>
    </div>

    <div>
      <input type="checkbox" id="violet" class="offscreen" />
      <label for="violet" class="switch purple"></label>
    </div>

    <div>
      <input type="checkbox" id="pink" class="offscreen" />
      <label for="pink" class="switch pink"></label>
    </div>
  </body>
</html>
```
