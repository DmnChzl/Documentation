# Outer Click

## useOuterClick

```ts
import { useEffect, useRef, type RefObject } from "react";

export default function useOuterClick<T extends HTMLElement>(onClick: (event: MouseEvent) => void): RefObject<T> {
  const ref = useRef<T>(null);

  const handleMouseDown = (event: MouseEvent) => {
    if (!ref.current || ref.current.contains(event.target as Node)) {
      return;
    }
    onClick(event);
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleMouseDown);
    return () => document.removeEventListener("mousedown", handleMouseDown);
  }, [ref, onClick]);

  return ref;
}
```
