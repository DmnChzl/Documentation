# Is Empty

## isEmpty.js

```js
export const isEmpty = obj => {
  if (typeof obj !== "object") {
    throw new Error("Not an object");
  }

  return Object.entries(obj).length === 0;
};
```

## isEmpty.test.js

```js
describe("isEmpty", () => {
  it("Should return true", () => {
    expect(isEmpty({})).toBe(true);
  });

  it("Should return false", () => {
    expect(isEmpty({ key: "value" })).toBe(false);
  });

  it("Should throw error", () => {
    try {
      isEmpty(42);
    } catch (err) {
      expect(err).toEqual("Not an object");
    }
  });
});
```
