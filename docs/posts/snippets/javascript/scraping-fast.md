# Web Scraping Fast

Using [Puppeteer](https://github.com/puppeteer/puppeteer)

## scrapingFast.js

```js
const cheerio = require("cheerio");
const puppeteer = require("puppeteer");

function delay(time) {
  return new Promise(resolve => {
    setTimeout(resolve, time);
  });
}

/**
 * Using jQuery Core to scrape data
 *
 * @param {string} content DOMContent
 * @returns {string} Speed Value
 */
function scrapeSpeedValue(content) {
  const $ = cheerio.load(content);
  return $("#speed-value").text();
}

/**
 * Using Puppeteer to go to website
 */
async function goFast() {
  const browser = await puppeteer.launch();

  const page = await browser.newPage();
  await page.goto("https://fast.com", {
    waitUntil: "networkidle0"
  });
  // await delay(10000);
  const content = await page.content();

  const speedValue = scrapeSpeedValue(content);
  console.log("speedValue:", speedValue);
}

goFast();
```
