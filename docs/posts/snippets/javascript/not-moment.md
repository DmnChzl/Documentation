# Not Moment

**DateTime** Extends **Date**

## dateTime.js

```js
const DAYS = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

const MONTHS = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

/**
 * @method strToDate
 * @param {String|Date} strDate Date (As String)
 * @param {RegExp} re RegExp (As Separator)
 * @returns {Date}
 */
const strToDate = (strDate, re = /[+,-./:\\_|]/) => {
  if (typeof strDate === "string") {
    const utcDate = strDate.split(re).reverse().join("-");
    return new Date(utcDate);
  }
  return strDate;
};

/**
 * @class DateTime
 * @param {String|Date} dateTime Date (21/05/1993)
 */
class DateTime extends Date {
  constructor(dateTime = new Date()) {
    dateTime = strToDate(dateTime);
    super(dateTime);
  }

  getProperMonth() {
    return this.getMonth() + 1;
  }

  setProperMonth(value) {
    this.setMonth(value - 1);
  }

  format(formatPattern = "DD/MM/YYYY") {
    const dayOfWeek = this.getDay();
    const dayOfMonth = this.getDate();
    const month = this.getProperMonth();
    const year = this.getFullYear();
    const hours = this.getHours();
    const minutes = this.getMinutes();
    const seconds = this.getSeconds();

    return formatPattern
      .replace("DDD", DAYS[dayOfWeek - 1])
      .replace("MMM", MONTHS[month - 1])
      .replace("DD", `${dayOfMonth}`.padStart(2, "0"))
      .replace("MM", `${month}`.padStart(2, "0"))
      .replace("YYYY", year)
      .replace("HH", `${hours}`.padStart(2, "0"))
      .replace("mm", `${minutes}`.padStart(2, "0"))
      .replace("ss", `${seconds}`.padStart(2, "0"));
  }

  isBefore(dateTime = new Date()) {
    dateTime = strToDate(dateTime);
    return this.getTime() < dateTime.getTime();
  }

  isAfter(dateTime = new Date()) {
    dateTime = strToDate(dateTime);
    return this.getTime() > dateTime.getTime();
  }

  /**
   * @method add
   * @param {Number} value Time Value (Default: 0)
   * @param {String} key Enum Key ('DAYS', 'MONTHS', 'YEARS', 'HOURS', 'MINUTES', 'SECONDS')
   * @returns {DateTime}
   */
  add(value = 0, key) {
    switch (key) {
      case "days":
      case "DAYS":
        this.setDate(this.getDate() + value);
        break;
      case "months":
      case "MONTHS":
        this.setProperMonth(this.getProperMonth() + value);
        break;
      case "years":
      case "YEARS":
        this.setFullYear(this.getFullYear() + value);
        break;
      case "hours":
      case "HOURS":
        this.setHours(this.getHours() + value);
        break;
      case "minutes":
      case "MINUTES":
        this.setMinutes(this.getMinutes() + value);
        break;
      case "seconds":
      case "SECONDS":
        this.setSeconds(this.getSeconds() + value);
        break;
      default:
        this.setTime(this.getTime() + value);
    }

    return this;
  }

  /**
   * @method del
   * @param {Number} value Time Value (Default: 0)
   * @param {String} key Enum Key ('DAYS', 'MONTHS', 'YEARS', 'HOURS', 'MINUTES', 'SECONDS')
   * @returns {DateTime}
   */
  del(value = 0, key) {
    switch (key) {
      case "days":
      case "DAYS":
        this.setDate(this.getDate() - value);
        break;
      case "months":
      case "MONTHS":
        this.setProperMonth(this.getProperMonth() - value);
        break;
      case "years":
      case "YEARS":
        this.setFullYear(this.getFullYear() - value);
        break;
      case "hours":
      case "HOURS":
        this.setHours(this.getHours() - value);
        break;
      case "minutes":
      case "MINUTES":
        this.setMinutes(this.getMinutes() - value);
        break;
      case "seconds":
      case "SECONDS":
        this.setSeconds(this.getSeconds() - value);
        break;
      default:
        this.setTime(this.getTime() - value);
    }

    return this;
  }
}

export default DateTime;
```

## dateTime.test.js

```js
import DateTime from "../dateTime";

describe("DateTime", () => {
  it("Instance Of Date", () => {
    const dateTime = new DateTime();
    expect(dateTime.getTime()).toEqual(new Date(dateTime).getTime());
  });

  it("Format", () => {
    const dateTime = new DateTime("21/05/1993");
    expect(dateTime.format("YYYY-MM-DD")).toEqual("1993-05-21");
    // NOTE: GMT +2
    expect(dateTime.format("HH:mm:ss")).toEqual("02:00:00");
    expect(dateTime.format("the DDD DDth MMM YYYY at HHhmm")).toEqual("the Friday 21th May 1993 at 02h00");
  });

  describe("Add Function", () => {
    it("Days", () => {
      const dateTime = new DateTime("21/05/1993");
      dateTime.add(3, "DAYS");
      expect(dateTime.getDate()).toEqual(24);
    });

    it("Months", () => {
      const dateTime = new DateTime("21/05/1993");
      dateTime.add(3, "MONTHS");
      expect(dateTime.getMonth()).toEqual(7);
    });

    it("Years", () => {
      const dateTime = new DateTime("21/05/1993");
      dateTime.add(3, "YEARS");
      expect(dateTime.getFullYear()).toEqual(1996);
    });

    it("Hours", () => {
      const dateTime = new DateTime("21/05/1993");
      dateTime.add(3, "HOURS");
      // NOTE: GMT +2
      expect(dateTime.getHours()).toEqual(5);
    });

    it("Minutes", () => {
      const dateTime = new DateTime("21/05/1993");
      dateTime.add(3, "MINUTES");
      expect(dateTime.getMinutes()).toEqual(3);
    });

    it("Seconds", () => {
      const dateTime = new DateTime("21/05/1993");
      dateTime.add(3, "SECONDS");
      expect(dateTime.getSeconds()).toEqual(3);
    });
  });

  describe("Del Function", () => {
    it("Days", () => {
      const dateTime = new DateTime("21/05/1993");
      dateTime.del(3, "DAYS");
      expect(dateTime.getDate()).toEqual(18);
    });

    it("Months", () => {
      const dateTime = new DateTime("21/05/1993");
      dateTime.del(3, "MONTHS");
      expect(dateTime.getMonth()).toEqual(1);
    });

    it("Years", () => {
      const dateTime = new DateTime("21/05/1993");
      dateTime.del(3, "YEARS");
      expect(dateTime.getFullYear()).toEqual(1990);
    });

    it("Hours", () => {
      const dateTime = new DateTime("21/05/1993");
      dateTime.del(3, "HOURS");
      // NOTE: GMT +2
      expect(dateTime.getHours()).toEqual(23);
    });

    it("Minutes", () => {
      const dateTime = new DateTime("21/05/1993");
      dateTime.del(3, "MINUTES");
      expect(dateTime.getMinutes()).toEqual(57);
    });

    it("Seconds", () => {
      const dateTime = new DateTime("21/05/1993");
      dateTime.del(3, "SECONDS");
      expect(dateTime.getSeconds()).toEqual(57);
    });
  });

  it("isBefore", () => {
    const dateTime = new DateTime("21/05/1993");
    expect(dateTime.del(2, "hours").isBefore(new DateTime("21/05/1993"))).toBe(true);
  });

  it("isAfter", () => {
    const dateTime = new DateTime("21/05/1993");
    expect(dateTime.add(2, "hours").isAfter(new DateTime("21/05/1993"))).toBe(true);
  });
});
```
