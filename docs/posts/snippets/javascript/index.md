---
layout: doc

prev:
  text: "Singleton"
  link: "/posts/snippets/java/singleton"
next:
  text: "Curry"
  link: "/posts/snippets/javascript/curry"
---

# JavaScript

JavaScript 💛 is a "dynamically typed", interpreted programming language primarily used for web development. Created in 1995 by **Brendan Eich** at **Netscape**, it is now one of the most popular languages, running both client-side and server-side thanks to environments like **NodeJS**.
