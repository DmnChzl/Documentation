# UUID V4

**U**niversally **U**nique **I**dentifier

## uuid.js

```js
/*
 * UUID V4
 */
const uuid = () => {
  const PATTERN = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx";

  return PATTERN.replace(/[xy]/g, char => {
    const random = (Math.random() * 16) | 0,
      idx = char === "x" ? random : (random & 0x3) | 0x8;
    return idx.toString(16);
  });
};
```
