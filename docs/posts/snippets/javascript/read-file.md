# Read File

## fileUtils.js

```js
/**
 * Read file from browser
 *
 * @param {File} file
 * @returns {Promise}
 */
const readFile = file => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.addEventListener("load", event => {
      if (event.target && event.target.result) {
        resolve(event.target.result);
      } else {
        reject(new Error("Unreadable"));
      }
    });

    reader.readAsText(file);
  });
};

const jsonFile = new File([JSON.stringify({ hello: "world" })], "hello_world.json", { type: "application/json" });
readFile(jsonFile).then(text => console.log(JSON.parse(text)));
```
