# Download File

## fileUtils.js

```js
export const createFile = (blobPart, fileName, type) => new File([blobPart], fileName, { type });

/**
 * Download file from browser
 *
 * @param {File} file
 */
export const downloadFile = file => {
  const anchor = document.createElement("a");
  const url = window.URL.createObjectURL(file);

  anchor.href = url;
  anchor.download = file.name;

  document.body.appendChild(anchor);
  anchor.click();
  document.body.removeChild(anchor);

  window.URL.revokeObjectURL(url);
};
```
