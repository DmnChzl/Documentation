# Internationalization

Using **R**evealing **M**odule **P**attern (RMP)

## i18n.js

```js
const i18n = (() => {
  /**
   * Privates Variables
   */
  let _locale = "";
  let _messages = {};

  /**
   * Set (Private) Locale
   * @param {string} locale
   */
  function setLocale(locale) {
    this._locale = locale;
  }

  /**
   * Set All (Private) Messages
   * @param {string[]} messages
   */
  function setMessages(messages) {
    this._messages = messages;
  }

  /**
   * Get Message (From Locale)
   * @param {string} key Message Key
   * @param {*} options
   * @returns {string} (Evaluated) Message
   */
  function getMessage(key, options) {
    const message = this._messages[this._locale][key] || "";
    // Replace All {{value}} With 'options.value'
    return message.replace(/{{(.+?)}}/g, (_, group) => options[group] || group);
  }

  /**
   * Public Methods
   */
  return {
    setLocale,
    setMessages,
    getMessage
  };
})();
```

## i18n.test.js

```js
describe("i18n", () => {
  const MESSAGES = {
    en: {
      "Hello.Friend": "Hello Friend!",
      "Hello.Name": "Hello {{name}}!"
    },
    fr: {
      "Hello.Friend": "Salut, l'ami !",
      "Hello.Name": "Salut, {{name}} !"
    }
  };

  beforeEach(() => {
    i18n.setMessages(MESSAGES);
  });

  it("Should Returns Static (EN) Message", () => {
    i18n.setLocale("en");
    expect(i18n.getMessage("Hello.Friend")).toEqual("Hello Friend!");
  });

  it("Should Returns Dynamic (FR) Message", () => {
    i18n.setLocale("fr");
    expect(i18n.getMessage("Hello.Name", { name: "John" })).toEqual("Salut, John !");
  });
});
```
