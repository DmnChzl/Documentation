# Curry

## curry.js

```js
const curry = fn => {
  const func = (...args) => {
    let arr = [];

    if (args.length === fn.length) {
      return fn(...args);
    }

    arr = [...arr, ...args];

    const innerFunc = (...innerArgs) => {
      arr = [...arr, ...innerArgs];

      if (arr.length === fn.length) {
        return fn(...arr);
      }

      return innerFunc;
    };
    return innerFunc;
  };
  return func;
};
```

## curry.test.js

```js
test("curry", () => {
  const append = (a, b, c) => a + b + c;
  expect(append("I", "Am", "Stephen", "Curry")).toEqual("IAmStephenCurry");
  const curryAppend = curry(append);
  expect(curryAppend("I")("Am")("Stephen")("Curry")).toEqual("IAmStephenCurry");
});
```
