# Flat Map

## flatMap.js

```js
export const flatMap = arr => arr.reduce((acc, val) => [...acc, ...val], []);
```

## flatMap.test.js

```js
test("flatMap", () => {
  const array = [["A"], ["B"], ["C"]];

  expect(array.flatMap(val => val)).toEqual(["A", "B", "C"]);
  expect(flatMap(array)).toEqual(["A", "B", "C"]);
  expect(array.flatMap(val => val)).toEqual(flatMap(array));
});
```
