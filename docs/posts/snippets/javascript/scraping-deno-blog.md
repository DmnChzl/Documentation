# Web Scraping Deno Blog

Using [Puppeteer](https://github.com/puppeteer/puppeteer)

## scrapingDenoBlog.js

```js
const cheerio = require("cheerio");
const puppeteer = require("puppeteer");

/**
 * Using jQuery Core to scrape data
 *
 * @param {string} content DOMContent
 * @returns {string} Speed Value
 */
function scrapeBlogTitles(content) {
  const $ = cheerio.load(content);
  let blogTitles = [];

  $("h3").each((_, title) => {
    const blogTitle = $(title).text();
    blogTitles = [...blogTitles, blogTitle];
  });

  return blogTitles;
}

/**
 * Using Puppeteer to go to website
 */
async function goDenoBlog() {
  const browser = await puppeteer.launch();

  const page = await browser.newPage();
  await page.goto("https://deno.com/blog");
  const content = await page.content();

  await browser.close();

  scrapeBlogTitles(content).forEach(blogTitle => {
    console.log("blogTitle:", blogTitle);
  });
}

goDenoBlog();
```
