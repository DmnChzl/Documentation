# Lodash Object

**\_lodash** Demystified

## objectUtils.js

```js
/**
 * Creates an array of the own enumerable property names of object
 *
 * @param {*} obj The object to query
 * @return {Array} Returns the array of property names
 */
export const keys = obj => {
  if (typeof obj !== "object") throw new Error("NaO: Not an Object");

  let res = [];

  for (const prop in obj) {
    res = [...res, prop];
  }

  return res;
};

/**
 * Creates an array of the own enumerable string keyed property values of object
 *
 * @param {*} obj The object to query
 * @return {Array} Returns the array of property values
 */
export const values = obj => {
  if (typeof obj !== "object") throw new Error("NaO: Not an Object");

  let res = [];

  for (const prop in obj) {
    res = [...res, obj[prop]];
  }

  return res;
};

/**
 * Creates an array of own enumerable string keyed-value pairs for object
 *
 * @param {*} obj The object to query
 * @return {Array} Returns the key-value pairs
 */
export const entries = obj => {
  if (typeof obj !== "object") throw new Error("NaO: Not an Object");

  let res = [];

  for (const prop in obj) {
    res = [...res, [prop, obj[prop]]];
  }

  return res;
};

/**
 * Creates an object composed of the inverted keys and values of object
 * If object contains duplicate values, subsequent values overwrite property assignments of previous values
 *
 * @param {*} obj The object to invert
 * @return {*} Returns the new inverted object
 */
export const invert = obj => {
  if (typeof obj !== "object") throw new Error("NaO: Not an Object");

  let res = {};

  for (const prop in obj) {
    res = { ...res, [obj[prop]]: prop };
  }

  return res;
};

/**
 * Creates an object composed of the picked object properties
 *
 * @param {*} obj The source object
 * @param {string[]} props The property paths to pick
 * @return {*} Returns the new object
 */
export const pick = (obj, ...props) => {
  if (typeof obj !== "object") throw new Error("NaO: Not an Object");

  let res = {};

  for (const prop in props) {
    if (obj[prop]) res = { ...res, [prop]: obj[prop] };
  }

  return res;
};
```

## objectUtils.spec.js

```js
describe("Lodash Object", () => {
  const myObject = { a: "1", b: "2", c: "3" };

  it("Object - Keys", () => {
    const myObjectKeys = keys(myObject);

    expect(myObjectKeys).toHaveLength(3);
    expect(myObjectKeys).toEqual(["a", "b", "c"]);
  });

  it("Throw: Object - Keys", () => {
    try {
      keys("myObject");
    } catch (err) {
      expect(err.message).toEqual("NaO: Not an Object");
    }
  });

  it("Object - Values", () => {
    const myObjectValues = values(myObject);

    expect(myObjectValues).toHaveLength(3);
    expect(myObjectValues).toEqual(["1", "2", "3"]);
  });

  it("Object - Entries", () => {
    const myObjectEntries = entries(myObject);

    expect(myObjectEntries).toHaveLength(3);
    expect(myObjectEntries).toEqual([
      ["a", "1"],
      ["b", "2"],
      ["c", "3"]
    ]);
  });

  it("Object - Invert", () => {
    const myObjectInverted = invert(myObject);

    expect(myObjectInverted["2"]).toEqual("b");
  });

  it("Object - Pick", () => {
    const myObjectPicked = pick(myObject, "a", "c");

    expect(myObjectPicked.a).toEqual("1");
    expect(myObjectPicked.c).toEqual("3");
  });
});
```
