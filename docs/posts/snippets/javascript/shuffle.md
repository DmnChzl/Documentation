# Shuffle

**Fisher**–**Yates** Algorithm: [bost.ocks.org](https://bost.ocks.org/mike/shuffle/)

## shuffle.js

```js
function shuffle(array) {
  let desc = array.length,
    temp,
    asc;

  // While there remain elements to shuffle...
  while (desc) {
    // Pick a remaining element...
    asc = Math.floor(Math.random() * desc--);

    // And swap it with the current element
    temp = array[desc];
    array[desc] = array[asc];
    array[asc] = temp;
  }

  return array;
}
```
