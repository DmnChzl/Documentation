# Remark Gist

## remarkGist.js

```js
import { visit } from "unist-util-visit";

export default function remarkGist() {
  return (tree, file) => {
    visit(tree, node => {
      if (node.type === "textDirective" || node.type == "leafDirective" || node.type === "containerDirective") {
        if (node.name !== "github") return;

        const data = node.data || (node.data = {});
        const attributes = node.attributes || {};
        const id = attributes.id;

        if (node.type === "textDirective") file.fail("Text directives for 'GitHub' not supported", node);
        if (!id) file.fail("Missing gist ID", node);

        data.hName = "iframe";
        data.hProperties = {
          src: `https://gist.github.com/${id}`,
          width: 720,
          height: "100%",
          frameBorder: 0
        };
      }
    });
  };
}
```
