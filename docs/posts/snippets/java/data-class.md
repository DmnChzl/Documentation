# Data Class

Immutable Class Since Java 17

## Application.java

```java
class Pokemon {
  private int id;
  private String name;
  private String type;

  private Pokemon(int id, String name, String type) {
    this.id = id;
    this.name = name;
    this.type = type;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "{ " + "id: " + id + ", name: " + name + ", type: " + type + " }";
  }
}

class Application {
  public static void main(String[] args) {
    Pokemon pkmn = new Pokemon(25, "Pikachu", "Electric");
    System.out.println(pkmn.toString()); // { id: 25, name: "Pikachu", type: "Electric" }
  }
}
```

## ApplicationWithRecord.java

```java
record Pokemon(int id, String name, String type) {
  @Override
  public String toString() {
    return "{ " + "id: " + id + ", name: " + name + ", type: " + type + " }";
  }
}

class ApplicationWithRecord {
  public static void main(String[] args) {
    Pokemon pkmn = new Pokemon(25, "Pikachu", "Electric");
    System.out.println(pkmn.toString()); // { id: 25, name: "Pikachu", type: "Electric" }
  }
}
```
