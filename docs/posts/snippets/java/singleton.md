# Singleton

## Application.java

```java
class Singleton {
  private String title;
  private int strength;
  private String description;
  private int quantity;
  private String origin;
  private double price;

  public Singleton() {}

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getStrength() {
    return strength;
  }

  public void setStrength(int strength) {
    this.strength = strength;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }
}

class Application {
  public static void main(String[] args) {
    Singleton whisky = new Singleton();
    whisky.setTitle("Singleton");
    whisky.setStrength(40);
    whisky.setQuantity(70);
    whisky.setOrigin("Ecosse/Speyside");
    whisky.setPrice(31.50);

    Singleton singleMaltWhisky = new Singleton();
    whisky.setDescription("Single Malt Whisky");

    System.out.println(whisky == singleMaltWhisky); // false
  }
}
```

## SingletonApplication.java

```java
final class Singleton {

  private static Singleton INSTANCE;

  private String title;
  private int strength;
  private String description;
  private int quantity;
  private String origin;
  private double price;

  private Singleton() {}

  public static Singleton getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new Singleton();
    }

    return INSTANCE;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getStrength() {
    return strength;
  }

  public void setStrength(int strength) {
    this.strength = strength;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }
}

class SingletonApplication {
  public static void main(String[] args) {
    Singleton whisky = Singleton.getInstance();
    whisky.setTitle("Singleton");
    whisky.setStrength(40);
    whisky.setQuantity(70);
    whisky.setOrigin("Ecosse/Speyside");
    whisky.setPrice(31.50);

    Singleton singleMaltWhisky = Singleton.getInstance();
    whisky.setDescription("Single Malt Whisky");

    System.out.println(whisky == singleMaltWhisky); // true
  }
}
```
