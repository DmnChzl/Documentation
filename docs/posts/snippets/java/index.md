---
layout: doc

prev:
  text: "Virtual DOM"
  link: "/posts/snippets/html/virtual-dom"
next:
  text: "Builder Pattern"
  link: "/posts/snippets/java/builder-pattern"
---

# Java

Java ☕ is an object-oriented programming language with "static typing" and an execution platform designed to be portable, secure, and efficient. It was developed by **Sun Microsystems** in 1995 and was maintained by **Oracle** (`javax.*`) until 2017, and then by the **Eclipse Foundation** (`jakarta.*`) up to the present day.
