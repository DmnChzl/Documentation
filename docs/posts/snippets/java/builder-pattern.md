# Builder Pattern

## Application.java

```java
class Pokemon {
  private int id;
  private String name;
  private String type;

  private Pokemon(int id, String name, String type) {
    this.id = id;
    this.name = name;
    this.type = type;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "{ " + "id: " + id + ", name: " + name + ", type: " + type + " }";
  }
}

class Application {
  public static void main(String[] args) {
    Pokemon pkmn = new Pokemon(25, "Pikachu", "Electric");
    System.out.println(pkmn.toString()); // { id: 25, name: "Pikachu", type: "Electric" }
  }
}
```

## BuilderApplication.java

```java
class Pokemon {
  private int id;
  private String name;
  private String type;

  private Pokemon(Builder builder) {
    this.id = builder.id;
    this.name = builder.name;
    this.type = builder.type;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getType() {
    return type;
  }

  @Override
  public String toString() {
    return "{ " + "id: " + id + ", name: " + name + ", type: " + type + " }";
  }

  public static class Builder {
    private int id;
    private String name;
    private String type;

    public Builder withId(int id) {
      this.id = id;
      return this;
    }

    public Builder withName(String name) {
      this.name = name;
      return this;
    }

    public Builder withType(String type) {
      this.type = type;
      return this;
    }

    public Pokemon build() {
      return new Pokemon(this);
    }
  }
}

class BuilderApplication {
  public static void main(String[] args) {
    Pokemon pkmn = new Pokemon.Builder()
      .withId(25)
      .withName("Pikachu")
      .withType("Electric")
      .build();

    System.out.println(pkmn.toString()); // { id: 25, name: "Pikachu", type: "Electric" }
  }
}
```
