# Bash Customization

## .bash_profile

```bash
if [ -f ~/.bashrc ]; then
    source  ~/.bashrc;
fi
```

## .bashrc

```bash
# Make Dir & Change Dir
function mkcd {
    mkdir "$1"
    cd "$_"
}

# Kick PID By Port
function kick {
    kill $(lsof -t -i:$1) && echo "PID $_ Kicked!"
}

git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

explicit_whoami() {
    WHOAMI=$(whoami)

    if [[ $WHOAMI == "azerty" ]]
        then
            echo "dmnchzl"
        else
            echo $WHOAMI
    fi
}

explicit_hostname() {
    HOSTNAME=$(hostname)

    if [[ $HOSTNAME == *"qwerty"* ]]
        then
            echo "localhost"
        else
            echo $HOSTNAME
    fi
}

prompt() {
    # [12:00] dmnchzl@localhost:~/workspace/project (main) $
    export PS1="[\[$(tput sgr0)\]\[\033[38;5;10m\]\t\[$(tput sgr0)\]] \[$(tput sgr0)\]\[\033[38;5;13m\]\$(explicit_whoami)\[$(tput sgr0)\]@\[$(tput sgr0)\]\[\033[38;5;13m\]\$(explicit_hostname)\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;11m\]\w\[$(tput sgr0)\]\[\033[38;5;9m\]\$(git_branch)\[$(tput sgr0)\] \\$ \[$(tput sgr0)\]"
}

prompt

# Aliases
alias ll='ls -l'
alias la='ll -a'
alias open=explorer
alias grep='grep --color'
alias nls='npm ls -g --depth 0'
alias jwt='openssl rand 64 | base64'
```
