# MongoDB

## Launch MongoDB with a database path

```shell
mongod --dbpath "C:/Program Files/MongoDB/Data/DataBase"
```

## Launch the Mongo Client

```shell
mongo
```

## See the actions available on the database

```shell
db.helps()
```

## See available databases

```shell
show dbs
```

## Use a database

```shell
use dmnchzl
```

## See the database used

```shell
db
```

## See the collections available in the database

```shell
show collections
```

## Create documents

```shell
db.apps.insert({_id:1, label:"Torch", color:"Red", available:false})
db.apps.insert({_id:2, label:"Decode", color:"Red", available:false})
db.apps.insert({_id:3, label:"PadLock", color:"Orange", available:false})
```

## See the content of the collection

```shell
db.apps.find()
{_id:1, label:"Torch", color:"Red", available:false}
{_id:2, label:"Decode", color:"Red", available:false}
{_id:3, label:"PadLock", color:"Orange", available:false}
```

## Add a property to a document

```shell
db.apps.update({label:"Torch"}, {$set: {tab:["A","B","C"]}})
```

## Edit a property in multiple documents

```shell
db.apps.update({available:false}, {$set: {available:true}}, {multi:true})
```

## View table contents in descending order

```shell
db.apps.find({label: {$in: ["Torch","Decode","PadLock"]}}).sort({_id:-1})
{_id:3, label:"PadLock", color:"Orange", available:true}
{_id:2, label:"Decode", color:"Red", available:true}
{_id:1, label:"Torch", color:"Red", available:true, tab:["A","B","C"]}
```

## Add a value to an array

```shell
db.apps.update({label:"Torch"}, {$push: {tab:"D"}})
```

## Add the same value in an array without duplicate

```shell
db.apps.update({label:"Torch"}, {$addToSet: {tab:"D"}})
```

## View the element owning the array properly

```shell
db.apps.find({label:"Torch"}).pretty()
{
     _id:1,
     label:"Torch",
     color:"Red",
     available:true,
     tab:[
         "A",
         "B",
         "C",
         "D"
     ]
}
```

## Remove the last value from an array

```shell
db.apps.update({label:"Torch"}, {$pop: {tab:1}})
```

## Delete a property in a document

```shell
db.apps.update({label:"Torch"}, {$unset: {tab:1}})
```

## Delete a document from a collection

```shell
db.apps.remove({label:"Torch"})
```

## Delete a collection

```shell
db.apps.drop()
```

## Delete a database

```shell
db.runCommand({dropDatabase:1})
```
