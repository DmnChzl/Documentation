# Linux

## Display "Hello"

```shell
echo "Hello" && echo 'World !'
```

## Write a file

```shell
# nano file.txt
vim file.txt
```

## Overwrite the file content and add a new line

```shell
echo "Erase content" > file.txt && echo "Add this" >> file.txt
```

## Read the file content

```shell
cat -n file.txt
```

## Search in a file

```shell
grep -n "this" file.txt
```

## Request "ROOT" rights

```shell
sudo su
```

## Unzip a zip

```shell
tar -zxvf tar.gz
```

## Force deletion of a file

```shell
rm -rf document.txt
```

## Copy a file

```shell
cp file.txt /home/user/download/file.txt
```

## Move a file

```shell
mv file.txt /home/user/download/document.txt
```

## Configure the keyboard in "AZERTY"

```shell
setxkbmap fr
```

## View command history

```shell
cat .bash_history
```

## View "NGINX" error logs

```shell
cat /var/log/nginx/error.log
```

## See running "NGINX" processes

```shell
ps aux | grep nginx
```

## Create a basic authentication file

```shell
sudo sh -c "echo -n 'username:' >> /etc/nginx/.htpasswd"
```

## Add password to basic authentication file

```shell
sudo sh -c "openssl passwd -apr1 >> /etc/nginx/.htpasswd"
```

## Get current date

```shell
date
```

## Show formatted date

```shell
echo "$(date + '%d/%m/%Y %H:%M')"
```

## Edit scheduled tasks

```shell
crontab -e
```

## Run a script every day at midnight

```shell
# m h dom mon dow command
0 0 * * * /home/script.sh
```

## File "DOS" To "UNIX"

```
sed -i 's/\r//'
```

## Transfer a file via SSH

```shell
scp -r -p /path/to/file user@host:/path/to/dest
```

## Count the number of lines in a "GIT" project

```shell
git ls-files | xargs cat | wc -l
```

## Count the number of 'JavaScript' lines in a "GIT" project / per file

```shell
git ls-files *.js | xargs wc -l
```
