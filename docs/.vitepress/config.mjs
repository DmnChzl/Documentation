import { defineConfig } from "vitepress";

const frConfig = {
  label: "French",
  lang: "fr",
  themeConfig: {
    nav: [
      {
        text: "Extraits de Code",
        items: [
          { text: "Bash", link: "/posts/snippets/bash/bash-custom" },
          { text: "HTML", link: "/fr/posts/snippets/html" },
          { text: "Java", link: "/fr/posts/snippets/java" },
          { text: "JavaScript", link: "/fr/posts/snippets/javascript" },
          { text: "React", link: "/posts/snippets/react/outer-click" },
          { text: "SQL", link: "/fr/posts/notes/sql" },
          { text: "TypeScript", link: "/fr/posts/snippets/typescript" }
        ]
      },
      { text: "Projets", link: "/projects" }
    ],
    sidebar: {
      "/fr/posts/": [
        {
          text: "Extraits de Code",
          items: [
            {
              text: "Bash",
              items: [
                { text: "Bash Customization", link: "/posts/snippets/bash/bash-custom" },
                { text: "Commit Message", link: "/posts/snippets/bash/commit-msg" }
              ],
              collapsed: true
            },
            {
              text: "HTML",
              link: "/fr/posts/snippets/html",
              items: [
                { text: "File Upload", link: "/posts/snippets/html/file-upload" },
                { text: "Spinner", link: "/posts/snippets/html/spinner" },
                { text: "Toggle Switch", link: "/posts/snippets/html/toggle-switch" },
                { text: "Virtual DOM", link: "/posts/snippets/html/virtual-dom" }
              ],
              collapsed: true
            },
            {
              text: "Java",
              link: "/fr/posts/snippets/java",
              items: [
                { text: "Builder Pattern", link: "/posts/snippets/java/builder-pattern" },
                { text: "Data Class", link: "/posts/snippets/java/data-class" },
                { text: "Singleton", link: "/posts/snippets/java/singleton" }
              ],
              collapsed: true
            },
            {
              text: "JavaScript",
              link: "/fr/posts/snippets/javascript",
              items: [
                { text: "Curry", link: "/posts/snippets/javascript/curry" },
                { text: "Data Toggle Expand", link: "/posts/snippets/javascript/data-toggle-expand" },
                { text: "Download File", link: "/posts/snippets/javascript/download-file" },
                { text: "Internationalization", link: "/posts/snippets/javascript/internationalization-rmp" },
                { text: "Is Empty", link: "/posts/snippets/javascript/is-empty" },
                { text: "Lodash Object", link: "/posts/snippets/javascript/lodash-object" },
                { text: "Not Moment", link: "/posts/snippets/javascript/not-moment" },
                { text: "Permutations", link: "/posts/snippets/javascript/permutations" },
                { text: "Read File", link: "/posts/snippets/javascript/read-file" },
                { text: "Remark Gist", link: "/posts/snippets/javascript/remark-gist" },
                { text: "Web Scraping Deno Blog", link: "/posts/snippets/javascript/scraping-deno-blog" },
                { text: "Web Scraping Fast", link: "/posts/snippets/javascript/scraping-fast" },
                { text: "Shuffle", link: "/posts/snippets/javascript/shuffle" },
                { text: "Spiral Matrix", link: "/posts/snippets/javascript/spiral-matrix" },
                { text: "Traversal", link: "/posts/snippets/javascript/traversal" },
                { text: "ULID", link: "/posts/snippets/javascript/ulid" },
                { text: "UUID", link: "/posts/snippets/javascript/uuid" }
              ],
              collapsed: true
            },
            {
              text: "React",
              items: [
                { text: "Outer Click", link: "/posts/snippets/react/outer-click" },
                { text: "Window Size", link: "/posts/snippets/react/window-size" }
              ],
              collapsed: true
            },
            {
              text: "TypeScript",
              link: "/fr/posts/snippets/TypeScript",
              items: [
                { text: "Binary Parser", link: "/posts/snippets/typescript/binary-parser" },
                { text: "BlockChain", link: "/posts/snippets/typescript/block-chain" },
                { text: "Cases", link: "/posts/snippets/typescript/cases" },
                { text: "DenoLand Server", link: "/posts/snippets/typescript/denoland-server" },
                { text: "Observer Pattern", link: "/posts/snippets/typescript/observer-pattern" },
                { text: "Proof Of Work", link: "/posts/snippets/typescript/proof-of-work" },
                { text: "Proxy Pattern", link: "/posts/snippets/typescript/proxy-pattern" }
              ],
              collapsed: true
            }
          ]
        },
        {
          text: "Notes",
          items: [
            {
              text: "Linux",
              link: "/fr/posts/notes/linux"
            },
            {
              text: "MongoDB",
              link: "/fr/posts/notes/mongo"
            },
            {
              text: "Spring Boot",
              link: "/fr/posts/notes/spring-boot"
            },
            {
              text: "Simple Query Language",
              link: "/fr/posts/notes/sql"
            }
          ]
        }
      ],
      "/fr/projects/": [
        {
          text: "Projets",
          link: "/fr/projects",
          items: [
            {
              text: "translate",
              link: "/fr/projects/translate"
            },
            {
              text: "Mocca (S)CSS - Unopinionated x Utility First CSS Library",
              link: "/fr/projects/mocca-css"
            },
            {
              text: "Staafe - Stay Safe",
              link: "/fr/projects/stay-safe"
            },
            {
              text: "DatePicker",
              link: "/fr/projects/datepicker"
            },
            {
              text: "Mocca Protocol",
              link: "/fr/projects/mocca-protocol"
            },
            {
              text: "[DIY] State Management - Solid 🌀",
              link: "/fr/projects/diy-context"
            },
            {
              text: "Fibonacci Card 🃏",
              link: "/fr/projects/fibonacci-card"
            },
            {
              text: "Minimal Google Map",
              link: "/fr/projects/mnml-gmap"
            },
            {
              text: "[DIY] State Management - Svelte 🌀",
              link: "/fr/projects/diy-stores"
            },
            {
              text: "Deno x Alosaur 🦕🦖",
              link: "/fr/projects/deno-alosaur"
            },
            {
              text: "Awesome Hooks",
              link: "/fr/projects/awesome-hooks"
            },
            {
              text: "Reminders 🔔",
              link: "/fr/projects/reminders"
            },
            {
              text: "PokeQL",
              link: "/fr/projects/pokeql"
            },
            {
              text: "MDWrapper",
              link: "/fr/projects/mdwrapper"
            }
          ]
        }
      ]
    },
    footer: {
      message: "Publié sous licence MIT.",
      copyright: "Fait avec ❤️ par Damien Chazoule"
    }
  }
};

// https://vitepress.dev/reference/site-config
export default defineConfig({
  base: "/",
  title: "Documentation",
  description: "Code Snippets, Projects 'n' Miscellaneous Notes",
  head: [["link", { rel: "icon", type: "image/svg+xml", href: "/favicon.svg" }]],
  // https://vitepress.dev/reference/default-theme-config
  themeConfig: {
    logo: "/favicon.svg",
    search: {
      provider: "local"
    },
    nav: [
      {
        text: "Code Snippets",
        items: [
          { text: "Bash", link: "/posts/snippets/bash/bash-custom" },
          { text: "HTML", link: "/posts/snippets/html" },
          { text: "Java", link: "/posts/snippets/java" },
          { text: "JavaScript", link: "/posts/snippets/javascript" },
          { text: "React", link: "/posts/snippets/react/outer-click" },
          { text: "SQL", link: "/posts/notes/sql" },
          { text: "TypeScript", link: "/posts/snippets/typescript" }
        ]
      },
      { text: "Projects", link: "/projects" }
    ],
    sidebar: {
      "/posts/": [
        {
          text: "Code Snippets",
          items: [
            {
              text: "Bash",
              items: [
                { text: "Bash Customization", link: "/posts/snippets/bash/bash-custom" },
                { text: "Commit Message", link: "/posts/snippets/bash/commit-msg" }
              ],
              collapsed: true
            },
            {
              text: "HTML",
              link: "/posts/snippets/html",
              items: [
                { text: "File Upload", link: "/posts/snippets/html/file-upload" },
                { text: "Spinner", link: "/posts/snippets/html/spinner" },
                { text: "Toggle Switch", link: "/posts/snippets/html/toggle-switch" },
                { text: "Virtual DOM", link: "/posts/snippets/html/virtual-dom" }
              ],
              collapsed: true
            },
            {
              text: "Java",
              link: "/posts/snippets/java",
              items: [
                { text: "Builder Pattern", link: "/posts/snippets/java/builder-pattern" },
                { text: "Data Class", link: "/posts/snippets/java/data-class" },
                { text: "Singleton", link: "/posts/snippets/java/singleton" }
              ],
              collapsed: true
            },
            {
              text: "JavaScript",
              link: "/posts/snippets/javascript",
              items: [
                { text: "Curry", link: "/posts/snippets/javascript/curry" },
                { text: "Data Toggle Expand", link: "/posts/snippets/javascript/data-toggle-expand" },
                { text: "Download File", link: "/posts/snippets/javascript/download-file" },
                { text: "Internationalization", link: "/posts/snippets/javascript/internationalization-rmp" },
                { text: "Is Empty", link: "/posts/snippets/javascript/is-empty" },
                { text: "Lodash Object", link: "/posts/snippets/javascript/lodash-object" },
                { text: "Not Moment", link: "/posts/snippets/javascript/not-moment" },
                { text: "Permutations", link: "/posts/snippets/javascript/permutations" },
                { text: "Read File", link: "/posts/snippets/javascript/read-file" },
                { text: "Remark Gist", link: "/posts/snippets/javascript/remark-gist" },
                { text: "Web Scraping Deno Blog", link: "/posts/snippets/javascript/scraping-deno-blog" },
                { text: "Web Scraping Fast", link: "/posts/snippets/javascript/scraping-fast" },
                { text: "Shuffle", link: "/posts/snippets/javascript/shuffle" },
                { text: "Spiral Matrix", link: "/posts/snippets/javascript/spiral-matrix" },
                { text: "Traversal", link: "/posts/snippets/javascript/traversal" },
                { text: "ULID", link: "/posts/snippets/javascript/ulid" },
                { text: "UUID", link: "/posts/snippets/javascript/uuid" }
              ],
              collapsed: true
            },
            {
              text: "React",
              items: [
                { text: "Outer Click", link: "/posts/snippets/react/outer-click" },
                { text: "Window Size", link: "/posts/snippets/react/window-size" }
              ],
              collapsed: true
            },
            {
              text: "TypeScript",
              link: "/posts/snippets/typescript",
              items: [
                { text: "Binary Parser", link: "/posts/snippets/typescript/binary-parser" },
                { text: "BlockChain", link: "/posts/snippets/typescript/block-chain" },
                { text: "Cases", link: "/posts/snippets/typescript/cases" },
                { text: "DenoLand Server", link: "/posts/snippets/typescript/denoland-server" },
                { text: "Observer Pattern", link: "/posts/snippets/typescript/observer-pattern" },
                { text: "Proof Of Work", link: "/posts/snippets/typescript/proof-of-work" },
                { text: "Proxy Pattern", link: "/posts/snippets/typescript/proxy-pattern" },
                { text: "Sort By Key", link: "/posts/snippets/typescript/sort-by-key" }
              ],
              collapsed: true
            }
          ]
        },
        {
          text: "Notes",
          items: [
            {
              text: "Linux",
              link: "/posts/notes/linux"
            },
            {
              text: "MongoDB",
              link: "/posts/notes/mongo"
            },
            {
              text: "Simple Query Language",
              link: "/posts/notes/sql"
            }
          ]
        }
      ],
      "/projects/": [
        {
          text: "Projects",
          link: "/projects",
          items: [
            {
              text: "Translate",
              link: "/projects/translate"
            },
            {
              text: "Mocca (S)CSS - Unopinionated x Utility First CSS Library",
              link: "/projects/mocca-css"
            },
            {
              text: "Staafe - Stay Safe",
              link: "/projects/stay-safe"
            },
            {
              text: "DatePicker",
              link: "/projects/datepicker"
            },
            {
              text: "Mocca Protocol",
              link: "/projects/mocca-protocol"
            },
            {
              text: "[DIY] State Management - Solid 🌀",
              link: "/projects/diy-context"
            },
            {
              text: "Fibonacci Card 🃏",
              link: "/projects/fibonacci-card"
            },
            {
              text: "Minimal Google Map",
              link: "/projects/mnml-gmap"
            },
            {
              text: "[DIY] State Management - Svelte 🌀",
              link: "/projects/diy-stores"
            },
            {
              text: "Deno x Alosaur 🦕🦖",
              link: "/projects/deno-alosaur"
            },
            {
              text: "Awesome Hooks",
              link: "/projects/awesome-hooks"
            },
            {
              text: "Reminders 🔔",
              link: "/projects/reminders"
            },
            {
              text: "PokeQL",
              link: "/projects/pokeql"
            },
            {
              text: "MDWrapper",
              link: "/projects/mdwrapper"
            }
          ]
        }
      ]
    },
    socialLinks: [
      { icon: "github", link: "https://gist.github.com/dmnchzl" },
      { icon: "gitlab", link: "https://gitlab.com/users/dmnchzl/snippets" }
    ],
    footer: {
      message: "Released under the MIT License.",
      copyright: `Make with ❤️ by Damien Chazoule`
    }
  },
  locales: {
    root: {
      label: "English",
      lang: "en"
    },
    fr: frConfig
  }
});
